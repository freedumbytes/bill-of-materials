package nl.demon.shadowland.freedumbytes.servlet.http.listener.session.timeout;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import lombok.extern.slf4j.Slf4j;


/**
 * A Session Timeout Logger.
 */
@WebListener("Session Timeout Logger")
@Slf4j
public class SessionTimeoutListener implements HttpSessionListener
{
  private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";


  @Override
  public void sessionCreated(HttpSessionEvent paramHttpSessionEvent)
  {
    if (paramHttpSessionEvent.getSession() != null)
    {
      log.info("Session with Id {} created.", paramHttpSessionEvent.getSession().getId());
    }
  }


  @Override
  public void sessionDestroyed(HttpSessionEvent paramHttpSessionEvent)
  {
    if (paramHttpSessionEvent.getSession() != null)
    {
      HttpSession httpSession = paramHttpSessionEvent.getSession();

      long createdTime = httpSession.getCreationTime();
      long lastAccessedTime = httpSession.getLastAccessedTime();
      int maxInactiveTime = httpSession.getMaxInactiveInterval();
      long currentTime = System.currentTimeMillis();
      boolean possibleSessionTimeout = (currentTime - lastAccessedTime) >= (maxInactiveTime * 1000L);

      log.info("Session with Id {} destroyed.", httpSession.getId());
      log.debug("Created Time       : {}", format(createdTime));
      log.debug("Last Accessed Time : {}", format(lastAccessedTime));
      log.debug("Current Time       : {}", format(currentTime));
      log.debug("Possible Timeout   : {}", possibleSessionTimeout);
    }
  }


  private static String format(long millis)
  {
    Instant instant = Instant.ofEpochMilli(millis);
    ZoneId systemDefaultTimeZone = ZoneId.systemDefault();
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

    return LocalDateTime.ofInstant(instant, systemDefaultTimeZone).format(dateTimeFormatter);
  }
}
