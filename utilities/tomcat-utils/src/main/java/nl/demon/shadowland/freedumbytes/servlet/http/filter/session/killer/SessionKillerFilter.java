package nl.demon.shadowland.freedumbytes.servlet.http.filter.session.killer;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import nl.demon.shadowland.freedumbytes.servlet.http.listener.session.timeout.SessionTimeoutListener;

import lombok.extern.slf4j.Slf4j;


/**
 * A sample filter to see {@link SessionTimeoutListener} in action.
 */
@WebFilter(filterName = "Session Killer", displayName = "SessionKiller", urlPatterns = { "/*" }, description = "Sample filter to see SessionTimeoutListener in action.")
@Slf4j
public class SessionKillerFilter implements Filter
{
  private static final int REST_TIMEOUT_SECONDS = 1;


  @Override
  public void init(FilterConfig filterConfig) throws ServletException
  {
    log.info("SessionKillerFilter created.");
  }


  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException
  {
    log.debug("SessionKillerFilter doFilter.");
    log.debug("Request       getProtocol = {}", request.getProtocol());
    log.debug("Request     getServerPort = {}", request.getServerPort());

    if (request instanceof HttpServletRequest)
    {
      HttpServletRequest httpServletRequest = (HttpServletRequest) request;

      log.debug("Request     getRequestURI = {}", httpServletRequest.getRequestURI());
      log.debug("Request    getServletPath = {}", httpServletRequest.getServletPath());
      log.debug("Request       getPathInfo = {}", httpServletRequest.getPathInfo());
      log.debug("Request getPathTranslated = {}", httpServletRequest.getPathTranslated());

      log.debug("Session found with getMaxInactiveInterval {}.", httpServletRequest.getSession().getMaxInactiveInterval());
      httpServletRequest.getSession().setMaxInactiveInterval(REST_TIMEOUT_SECONDS);
      log.debug("Session reset setMaxInactiveInterval to {}.", httpServletRequest.getSession().getMaxInactiveInterval());
    }

    filterChain.doFilter(request, response);
  }


  @Override
  public void destroy()
  {
    log.info("SessionKillerFilter destroyed.");
  }
}
