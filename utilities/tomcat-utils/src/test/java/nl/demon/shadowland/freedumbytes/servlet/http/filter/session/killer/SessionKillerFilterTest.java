package nl.demon.shadowland.freedumbytes.servlet.http.filter.session.killer;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;


@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class SessionKillerFilterTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(SessionKillerFilter.class);

  @Mock
  private FilterConfig filterConfig;

  @Mock
  private ServletRequest servletRequest;

  @Mock
  private ServletResponse servletResponse;

  @Mock
  private HttpServletRequest httpServletRequest;

  @Mock
  private HttpSession httpSession;

  @Mock
  private FilterChain filterChain;

  private SessionKillerFilter killerFilter = new SessionKillerFilter();


  @Test
  public void init() throws ServletException
  {
    killerFilter.init(filterConfig);

    LoggingEvent event = loggerRule.verifyLoggingEvent();

    assertThat(event.getLevel()).isEqualTo(Level.INFO);
    assertThat(event.getMessage()).isEqualTo("SessionKillerFilter created.");
  }


  @Test
  public void doFilterServletRequest() throws IOException, ServletException
  {
    when(servletRequest.getProtocol()).thenReturn("http");
    when(servletRequest.getServerPort()).thenReturn(80);

    killerFilter.doFilter(servletRequest, servletResponse, filterChain);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(3));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(0).getMessage()).isEqualTo("SessionKillerFilter doFilter.");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(1).getMessage()).isEqualTo("Request       getProtocol = http");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(2).getMessage()).isEqualTo("Request     getServerPort = 80");
  }


  @Test
  public void doFilterHttpServletRequest() throws IOException, ServletException
  {
    when(httpServletRequest.getProtocol()).thenReturn("https");
    when(httpServletRequest.getServerPort()).thenReturn(443);

    when(httpServletRequest.getRequestURI()).thenReturn("/webapp/servlet/info/index.html");
    when(httpServletRequest.getServletPath()).thenReturn("/servlet");
    when(httpServletRequest.getPathInfo()).thenReturn("/info/index.html");
    when(httpServletRequest.getPathTranslated()).thenReturn("/www/docs/info/index.html");

    when(httpServletRequest.getSession()).thenReturn(httpSession);
    when(httpSession.getMaxInactiveInterval()).thenReturn(300, 1);

    killerFilter.doFilter(httpServletRequest, servletResponse, filterChain);

    verify(httpSession).setMaxInactiveInterval(1);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(9));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(0).getMessage()).isEqualTo("SessionKillerFilter doFilter.");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(1).getMessage()).isEqualTo("Request       getProtocol = https");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(2).getMessage()).isEqualTo("Request     getServerPort = 443");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(3).getMessage()).isEqualTo("Request     getRequestURI = /webapp/servlet/info/index.html");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage()).isEqualTo("Request    getServletPath = /servlet");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(5).getMessage()).isEqualTo("Request       getPathInfo = /info/index.html");

    assertThat(events.get(6).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(6).getMessage()).isEqualTo("Request getPathTranslated = /www/docs/info/index.html");

    assertThat(events.get(7).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(7).getMessage()).isEqualTo("Session found with getMaxInactiveInterval 300.");

    assertThat(events.get(8).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(8).getMessage()).isEqualTo("Session reset setMaxInactiveInterval to 1.");
  }


  @Test
  public void destroy()
  {
    killerFilter.destroy();

    LoggingEvent event = loggerRule.verifyLoggingEvent();

    assertThat(event.getLevel()).isEqualTo(Level.INFO);
    assertThat(event.getMessage()).isEqualTo("SessionKillerFilter destroyed.");
  }
}
