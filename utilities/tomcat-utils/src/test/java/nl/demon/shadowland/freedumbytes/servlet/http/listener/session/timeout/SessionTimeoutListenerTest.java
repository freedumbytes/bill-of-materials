package nl.demon.shadowland.freedumbytes.servlet.http.listener.session.timeout;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;


@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class SessionTimeoutListenerTest
{
  private static final ZonedDateTime CREATED_TIME = ZonedDateTime.of(2018, 11, 20, 21, 01, 02, 321, ZoneId.systemDefault());
  private static final long CREATED_TIME_MILLIS = CREATED_TIME.toEpochSecond() * 1000;
  private static final ZonedDateTime LAST_ACCESSED_TIME = ZonedDateTime.of(2018, 11, 20, 22, 10, 20, 456, ZoneId.systemDefault());
  private static final long LAST_ACCESSED_TIME_MILLIS = LAST_ACCESSED_TIME.toEpochSecond() * 1000;
  private static final int MAX_INACTIVE_INTERVAL = 30 * 60;
  private static final ZonedDateTime CURRENT_TIME = ZonedDateTime.now(ZoneId.systemDefault());
  private static final long CURRENT_TIME_MILLIS = CURRENT_TIME.toEpochSecond() * 1000;

  @Rule
  public LoggerRule loggerRule = new LoggerRule(SessionTimeoutListener.class);

  @Mock
  private HttpSessionEvent httpSessionEvent;

  @Mock
  private HttpSession httpSession;

  private SessionTimeoutListener sessionTimeoutListener = new SessionTimeoutListener();


  @Test
  public void noSessionCreated() throws ServletException
  {
    when(httpSessionEvent.getSession()).thenReturn(null);

    sessionTimeoutListener.sessionCreated(httpSessionEvent);

    loggerRule.verifyLoggingEventNever();
  }


  @Test
  public void sessionCreated() throws ServletException
  {
    when(httpSessionEvent.getSession()).thenReturn(httpSession);
    when(httpSession.getId()).thenReturn("id#");

    sessionTimeoutListener.sessionCreated(httpSessionEvent);

    LoggingEvent event = loggerRule.verifyLoggingEvent();

    assertThat(event.getLevel()).isEqualTo(Level.INFO);
    assertThat(event.getMessage()).isEqualTo("Session with Id id# created.");
  }


  @Test
  public void noSessionDestroyed() throws ServletException
  {
    when(httpSessionEvent.getSession()).thenReturn(null);

    sessionTimeoutListener.sessionDestroyed(httpSessionEvent);

    loggerRule.verifyLoggingEventNever();
  }


  @Test
  public void oldSessionDestroyed() throws ServletException
  {
    when(httpSessionEvent.getSession()).thenReturn(httpSession);
    when(httpSession.getId()).thenReturn("id#");

    when(httpSession.getCreationTime()).thenReturn(CREATED_TIME_MILLIS);
    when(httpSession.getLastAccessedTime()).thenReturn(LAST_ACCESSED_TIME_MILLIS);
    when(httpSession.getMaxInactiveInterval()).thenReturn(MAX_INACTIVE_INTERVAL);

    sessionTimeoutListener.sessionDestroyed(httpSessionEvent);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(5));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(0).getMessage()).isEqualTo("Session with Id id# destroyed.");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(1).getMessage()).isEqualTo("Created Time       : 2018-11-20 21:01:02.000");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(2).getMessage()).isEqualTo("Last Accessed Time : 2018-11-20 22:10:20.000");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(3).getRenderedMessage()).startsWith("Current Time       : ");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage()).isEqualTo("Possible Timeout   : true");
  }


  @Test
  public void activeSessionDestroyedNow() throws ServletException
  {
    when(httpSessionEvent.getSession()).thenReturn(httpSession);
    when(httpSession.getId()).thenReturn("id#");

    when(httpSession.getCreationTime()).thenReturn(CREATED_TIME_MILLIS);
    when(httpSession.getLastAccessedTime()).thenReturn(CURRENT_TIME_MILLIS);
    when(httpSession.getMaxInactiveInterval()).thenReturn(MAX_INACTIVE_INTERVAL);

    sessionTimeoutListener.sessionDestroyed(httpSessionEvent);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(5));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(0).getMessage()).isEqualTo("Session with Id id# destroyed.");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(1).getMessage()).isEqualTo("Created Time       : 2018-11-20 21:01:02.000");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(2).getRenderedMessage()).startsWith("Last Accessed Time : ");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(3).getRenderedMessage()).startsWith("Current Time       : ");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage()).isEqualTo("Possible Timeout   : false");
  }
}
