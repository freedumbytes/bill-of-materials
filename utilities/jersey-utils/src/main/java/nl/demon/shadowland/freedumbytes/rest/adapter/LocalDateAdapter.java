package nl.demon.shadowland.freedumbytes.rest.adapter;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * LocalDate ISO date format {@code XmlAdapter} for custom marshaling.
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate>
{
  @Override
  public LocalDate unmarshal(String value)
  {
    if (value == null || value.trim().isEmpty())
    {
      return null;
    }
    else
    {
      return LocalDate.parse(value);
    }
  }


  @Override
  public String marshal(LocalDate value)
  {
    if (value == null)
    {
      return null;
    }
    else
    {
      return value.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
  }
}
