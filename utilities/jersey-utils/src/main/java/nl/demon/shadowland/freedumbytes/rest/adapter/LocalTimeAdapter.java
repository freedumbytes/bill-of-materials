package nl.demon.shadowland.freedumbytes.rest.adapter;


import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * LocalTime ISO time format {@code XmlAdapter} for custom marshaling.
 */
public class LocalTimeAdapter extends XmlAdapter<String, LocalTime>
{
  @Override
  public LocalTime unmarshal(String value)
  {
    if (value == null || value.trim().isEmpty())
    {
      return null;
    }
    else
    {
      return LocalTime.parse(value);
    }
  }


  @Override
  public String marshal(LocalTime value)
  {
    if (value == null)
    {
      return null;
    }
    else
    {
      return value.format(DateTimeFormatter.ISO_LOCAL_TIME);
    }
  }
}
