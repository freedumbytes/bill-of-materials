package nl.demon.shadowland.freedumbytes.rest.adapter;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * LocalDateTime ISO date-time format {@code XmlAdapter} for custom marshaling.
 */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime>
{
  @Override
  public LocalDateTime unmarshal(String value)
  {
    if (value == null || value.trim().isEmpty())
    {
      return null;
    }
    else
    {
      return LocalDateTime.parse(value);
    }
  }


  @Override
  public String marshal(LocalDateTime value)
  {
    if (value == null)
    {
      return null;
    }
    else
    {
      return value.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
  }
}
