package nl.demon.shadowland.freedumbytes.rest.param;


import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.ext.ParamConverter;


/**
 * LocalTime ISO time format {@code ParamConverter} of a message parameter value and the corresponding custom Java type.
 */
public class LocalTimeConverter implements ParamConverter<LocalTime>
{
  @Override
  public LocalTime fromString(String value)
  {
    if (value == null || value.trim().isEmpty())
    {
      return null;
    }
    else
    {
      return LocalTime.parse(value);
    }
  }


  @Override
  public String toString(LocalTime value)
  {
    if (value == null)
    {
      return null;
    }
    else
    {
      return value.format(DateTimeFormatter.ISO_LOCAL_TIME);
    }
  }
}
