package nl.demon.shadowland.freedumbytes.rest.param;


import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;


/**
 * Custom {@code ParamConverterProvider} container of several Java 8 Local Date/Time API ISO format {@code ParamConverter}s.
 */
@Provider
public class CustomParamConverterProvider implements ParamConverterProvider
{
  @Override
  @SuppressWarnings("unchecked")
  public <T> ParamConverter<T> getConverter(final Class<T> rawType, Type genericType, Annotation[] annotations)
  {
    if (LocalDate.class.isAssignableFrom(rawType))
    {
      return (ParamConverter<T>) new LocalDateConverter();
    }
    else if (LocalDateTime.class.isAssignableFrom(rawType))
    {
      return (ParamConverter<T>) new LocalDateTimeConverter();
    }
    else if (LocalTime.class.isAssignableFrom(rawType))
    {
      return (ParamConverter<T>) new LocalTimeConverter();
    }
    else
    {
      return null;
    }
  }
}
