package nl.demon.shadowland.freedumbytes.rest.swagger.filter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import io.swagger.v3.core.filter.AbstractSpecFilter;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class IncludeRootSpecFilter extends AbstractSpecFilter
{
  private static final String JSON_TYPE = "json";
  private static final String REF_SEPARATOR = "/";
  private static final String INCLUDE_ROOT_SCHEMA_NAME_POSTFIX = "IncludeRoot";
  private static final String TYPE_OBJECT = "object";
  private static final String INCLUDE_ROOT_WORKAROUND_DESCRIPTION_PREFIX = "Include root workaround: ";

  private Map<String, String> refsMap = new HashMap<>();


  @Override
  public Optional<OpenAPI> filterOpenAPI(OpenAPI openAPI, Map<String, List<String>> params, Map<String, String> cookies, Map<String, List<String>> headers)
  {
    if (openAPI.getPaths() != null)
    {
      openAPI.getPaths().values().forEach(this::scanPathItem);

      refsMap.keySet().forEach(key -> createSchema(openAPI.getComponents(), key));
    }

    return Optional.of(openAPI);
  }


  private void scanPathItem(PathItem pathItem)
  {
    pathItem.readOperations().forEach(this::scanOperation);
  }


  private void scanOperation(Operation operation)
  {
    if (operation.getRequestBody() != null)
    {
      scanRequestBody(operation.getRequestBody());
    }

    if (operation.getResponses() != null)
    {
      operation.getResponses().values().forEach(this::scanApiResponse);
    }
  }


  private void scanRequestBody(RequestBody requestBody)
  {
    scanContent(requestBody.getContent());
  }


  private void scanApiResponse(ApiResponse apiResponse)
  {
    scanContent(apiResponse.getContent());
  }


  private void scanContent(Content content)
  {
    if (content != null)
    {
      Map<String, MediaType> updatedMediaTypes = new HashMap<>();
      Map<String, MediaType> jsonMediaTypes = new HashMap<>();
      List<MediaType> nonJsonMediaTypes = new ArrayList<>();

      for (Map.Entry<String, MediaType> entry : content.entrySet())
      {
        if (entry.getKey().contains(JSON_TYPE))
        {
          jsonMediaTypes.put(entry.getKey(), entry.getValue());
        }
        else
        {
          nonJsonMediaTypes.add(entry.getValue());
        }
      }

      for (Map.Entry<String, MediaType> jsonEntry : jsonMediaTypes.entrySet())
      {
        boolean isMediaTypeReused = nonJsonMediaTypes.stream().anyMatch(nonJsonMediaType -> nonJsonMediaType == jsonEntry.getValue());
        log.debug("isMediaTypeReused:" + isMediaTypeReused);

        if (isMediaTypeReused)
        {
          checkReusedAssumption(jsonEntry.getValue());
        }

        MediaType updatedMediaType = createMediaTypeOrUpdateSchema(jsonEntry.getValue(), isMediaTypeReused);
        updatedMediaTypes.put(jsonEntry.getKey(), updatedMediaType);
      }

      updatedMediaTypes.entrySet().forEach(entry -> content.addMediaType(entry.getKey(), entry.getValue()));
    }
  }


  /**
   * When MediaType is reused the @ApiResponse is not used.
   *
   * Thus almost all properties will be null and we don't need to clone anything.
   *
   * @param mediaType
   */
  private void checkReusedAssumption(MediaType mediaType)
  {
    if (!isEmptyExclusiveSchema(mediaType) || !isEmptyExclusiveRef(mediaType.getSchema()))
    {
      log.warn("MediaType reuse assumption is false; Do we need to implement clone MediaType?");
    }
  }


  private boolean isEmptyExclusiveSchema(MediaType mediaType)
  {
    return (mediaType.getExamples() == null || mediaType.getExamples().isEmpty()) //
        && mediaType.getExample() == null //
        && (mediaType.getEncoding() == null || mediaType.getEncoding().isEmpty()) //
        && (mediaType.getExtensions() == null || mediaType.getExtensions().isEmpty());
  }


  private boolean isEmptyExclusiveRef(Schema<?> schema)
  {
    return schema.getDefault() == null //
        && schema.getName() == null //
        && schema.getTitle() == null //
        && schema.getMultipleOf() == null //
        && schema.getMaximum() == null //
        && schema.getExclusiveMaximum() == null //
        && schema.getMinimum() == null //
        && schema.getExclusiveMinimum() == null //
        && schema.getMaxLength() == null //
        && schema.getMinLength() == null //
        && schema.getPattern() == null //
        && schema.getMaxItems() == null //
        && schema.getMinItems() == null //
        && schema.getUniqueItems() == null //
        && schema.getMaxProperties() == null //
        && schema.getMinProperties() == null //
        && (schema.getRequired() == null || schema.getRequired().isEmpty()) //
        && schema.getType() == null //
        && schema.getNot() == null //
        && (schema.getProperties() == null || schema.getProperties().isEmpty()) //
        && schema.getAdditionalProperties() == null //
        && schema.getDescription() == null //
        && schema.getFormat() == null //
        && schema.getNullable() == null //
        && schema.getReadOnly() == null //
        && schema.getWriteOnly() == null //
        && schema.getExample() == null //
        && schema.getExternalDocs() == null //
        && schema.getDeprecated() == null //
        && schema.getXml() == null //
        && (schema.getExtensions() == null || schema.getExtensions().isEmpty()) //
        && (schema.getEnum() == null || schema.getEnum().isEmpty()) //
        && schema.getDiscriminator() == null;
  }


  /**
   * If not already handled then create when MediaType is reused otherwise update Schema reference.
   *
   * @param mediaType
   * @param isMediaTypeReused
   * @return
   */
  private MediaType createMediaTypeOrUpdateSchema(MediaType mediaType, boolean isMediaTypeReused)
  {
    MediaType result = mediaType;
    Schema<?> schema = mediaType.getSchema();
    String ref = schema.get$ref();

    if (!StringUtils.endsWith(ref, getIncludeRootSchemaNamePostfix()))
    {
      String key = StringUtils.substringAfterLast(ref, REF_SEPARATOR);

      if (!refsMap.containsKey(key))
      {
        refsMap.put(key, ref);
      }

      if (isMediaTypeReused)
      {
        result = createMediaType(schema);
      }
      else
      {
        updateSchema(schema);
      }
    }

    return result;
  }


  private MediaType createMediaType(Schema<?> schema)
  {
    MediaType mediaType = new MediaType();

    Schema<?> newSchema = new Schema<>();
    newSchema.set$ref(schema.get$ref() + getIncludeRootSchemaNamePostfix());

    mediaType.setSchema(newSchema);

    return mediaType;
  }


  private void updateSchema(Schema<?> schema)
  {
    schema.set$ref(schema.get$ref() + getIncludeRootSchemaNamePostfix());
  }


  private void createSchema(Components components, String key)
  {
    log.info("Create workaround for " + key);

    Schema<?> originalSchema = components.getSchemas().get(key);

    Schema<?> schema = new Schema<>();
    schema.setType(TYPE_OBJECT);

    if (StringUtils.isNotBlank(originalSchema.getDescription()))
    {
      schema.setDescription(getIncludeRootWorkaroundDescriptionPrefix() + originalSchema.getDescription());
    }

    String propKey = StringUtils.lowerCase(StringUtils.substring(key, 0, 1)) + StringUtils.substring(key, 1);

    if (originalSchema.getXml() != null && originalSchema.getXml().getName() != null)
    {
      propKey = originalSchema.getXml().getName();
    }

    Schema<?> propSchema = new Schema<>();
    propSchema.set$ref(refsMap.get(key));

    schema.addProperties(propKey, propSchema);

    components.addSchemas(key + getIncludeRootSchemaNamePostfix(), schema);
  }


  /**
   * Default IncludeRoot option workaround schema name postfix.
   *
   * @return Schema name postfix.
   */
  protected String getIncludeRootSchemaNamePostfix()
  {
    return INCLUDE_ROOT_SCHEMA_NAME_POSTFIX;
  }


  /**
   * Default IncludeRoot option workaround description prefix.
   *
   * @return Description prefix.
   */
  protected String getIncludeRootWorkaroundDescriptionPrefix()
  {
    return INCLUDE_ROOT_WORKAROUND_DESCRIPTION_PREFIX;
  }
}
