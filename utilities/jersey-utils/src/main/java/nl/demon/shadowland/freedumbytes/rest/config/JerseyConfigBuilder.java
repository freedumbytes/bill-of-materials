package nl.demon.shadowland.freedumbytes.rest.config;


import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.ext.ContextResolver;

import org.eclipse.persistence.jaxb.BeanValidationMode;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.oxm.XMLConstants;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.moxy.xml.MoxyXmlFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.bridge.SLF4JBridgeHandler;

import nl.demon.shadowland.freedumbytes.java.util.logging.jersey.Slf4jLoggingFeatureLevelWrapper;
import nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager;
import nl.demon.shadowland.freedumbytes.rest.swagger.filter.CustomSpecFilter;

import lombok.extern.slf4j.Slf4j;


/**
 * Often used Jersey Server/Client Configuration settings.
 */
@Slf4j
public class JerseyConfigBuilder
{
  private static final String LOGGING_FEATURE_ALTERNATIVE_NO_SLF4J_BRIDGE_HANDLER = "This LogTraffic and DumpEntity LoggingFeature Alternative doesn't require SLF4JBridgeHandler.";
  private static final String SLF4J_BRIDGE_HANDLER_ONLY_DURING_JERSEY_TEST = "Enable SLF4JBridgeHandler only during JerseyTest to log for example GrizzlyTestContainer, NetworkListener and HttpServer stuff as Slf4j.";
  private static final String LOGGING_FEATURE_ALTERNATIVE_WARN = LOGGING_FEATURE_ALTERNATIVE_NO_SLF4J_BRIDGE_HANDLER + " " + SLF4J_BRIDGE_HANDLER_ONLY_DURING_JERSEY_TEST;

  private static boolean enableSlf4jBridge;

  private boolean enableMoxyXmlFeature;
  private boolean enableMoxyJsonIncludeRoot;
  private boolean enableMoxyJsonFormattedOutput;
  private BeanValidationMode beanValidationMode = BeanValidationMode.NONE;
  private boolean enableBeanValidationSendErrorInResponse;
  private boolean enableLogTrafficDumpEntityLoggingFeatureWorkaround;
  private boolean enableLogTrafficDumpEntityLoggingFeatureAlternative;
  private Level logTrafficDumpEntityLoggingLevel;
  private boolean disableWadlFeature;


  /**
   * Bridging legacy APIs.
   *
   * <p>
   * The jul-to-slf4j.jar artifact includes a {@code java.util.logging} (jul) handler, namely {@code SLF4JBridgeHandler}, which routes all incoming jul records to the SLF4J API.
   * </p>
   * <p>
   * If you are concerned about application performance, then use of {@code SLF4JBridgeHandler} is appropriate only if any one of the following two conditions is true:
   * </p>
   *
   * <ol>
   * <li>few j.u.l. logging statements are in play;</li>
   * <li>{@code LevelChangePropagator} has been installed.</li>
   * </ol>
   *
   * @see <a href="https://www.slf4j.org/legacy.html" target="_blank" rel="noopener noreferrer">Bridging legacy APIs</a>
   */
  public static void globallyEnableSlf4jBridge()
  {
    if (Slf4jLogManager.isInstalled())
    {
      log.info("No need to install SLF4JBridgeHandler because Slf4jLogManager is activated.");
    }
    else
    {
      enableSlf4jBridge = true;

      log.info("Activate SLF4JBridgeHandler.");

      SLF4JBridgeHandler.removeHandlersForRootLogger();
      SLF4JBridgeHandler.install();
    }
  }


  /**
   * Using LoggingFeature workaround for Jersey Test Framework Issue 3502 with configuration properties:
   *
   * <ul>
   * <li>{@code LOG_TRAFFIC} property enables basic logging of the request and response flow on both - client and server.</li>
   * <li>{@code DUMP_ENTITY} property instructs the test traffic logger to dump message entities as part of the test traffic logging.</li>
   * </ul>
   *
   * <p>
   * This will disable {@link #enableLogTrafficDumpEntityLoggingFeatureAlternative() enableLogTrafficDumpEntityLoggingFeatureAlternative}.
   * </p>
   *
   * @see <a href="https://github.com/eclipse-ee4j/jersey/issues/3502" target="_blank" rel="noopener noreferrer">Issue 3502 - LOG_TRAFFIC and DUMP_ENTITY TestProperties not honored.</a>
   *
   * @return A self reference.
   */
  public JerseyConfigBuilder enableLogTrafficDumpEntityLoggingFeatureWorkaround()
  {
    enableLogTrafficDumpEntityLoggingFeatureWorkaround = true;
    enableLogTrafficDumpEntityLoggingFeatureAlternative = false;

    return this;
  }


  /**
   * Using LoggingFeature alternative for Jersey Test Framework {@link Slf4jLoggingFeatureLevelWrapper}.
   *
   * <p>
   * Will log when {@code org.glassfish.jersey.logging} is set to {@code DEBUG}.
   * </p>
   * <p>
   * This will disable {@link #enableLogTrafficDumpEntityLoggingFeatureWorkaround() enableLogTrafficDumpEntityLoggingFeatureWorkaround} and doesn't require {@link #globallyEnableSlf4jBridge() globallyEnableSlf4jBridge}.
   * </p>
   *
   * @return A self reference.
   */
  public JerseyConfigBuilder enableLogTrafficDumpEntityLoggingFeatureAlternative()
  {
    return enableLogTrafficDumpEntityLoggingFeatureAlternative(Level.FINE);
  }


  /**
   * Using LoggingFeature alternative for Jersey Test Framework {@link Slf4jLoggingFeatureLevelWrapper}.
   *
   * <p>
   * Will log when {@code org.glassfish.jersey.logging} is set to {@code logTrafficDumpEntityLoggingLevel} or lower.
   * </p>
   * <p>
   * This will disable {@link #enableLogTrafficDumpEntityLoggingFeatureWorkaround() enableLogTrafficDumpEntityLoggingFeatureWorkaround} and doesn't require {@link #globallyEnableSlf4jBridge() globallyEnableSlf4jBridge}.
   * </p>
   *
   * @param logTrafficDumpEntityLoggingLevel
   *          Level at which logging is activated.
   * @return A self reference.
   */
  public JerseyConfigBuilder enableLogTrafficDumpEntityLoggingFeatureAlternative(Level logTrafficDumpEntityLoggingLevel)
  {
    if (enableSlf4jBridge)
    {
      log.warn(LOGGING_FEATURE_ALTERNATIVE_WARN);
    }

    enableLogTrafficDumpEntityLoggingFeatureAlternative = true;
    enableLogTrafficDumpEntityLoggingFeatureWorkaround = false;
    this.logTrafficDumpEntityLoggingLevel = logTrafficDumpEntityLoggingLevel;

    return this;
  }


  /**
   * Feature used to register MOXy XML providers.
   *
   * @return A self reference.
   */
  public JerseyConfigBuilder enableMoxyXmlFeature()
  {
    enableMoxyXmlFeature = true;

    return this;
  }


  /**
   * Specify if the root node should be included in the JSON message (default is false). This will also {@link #enableMoxyXmlFeature() enableMoxyXmlFeature}.
   *
   * @return A self reference.
   */
  public JerseyConfigBuilder enableMoxyJsonIncludeRoot()
  {
    enableMoxyJsonIncludeRoot = true;
    enableMoxyXmlFeature();

    return this;
  }


  /**
   * Specify if the JSON output should be formatted (default is false). This will also {@link #enableMoxyXmlFeature() enableMoxyXmlFeature}.
   *
   * @return A self reference.
   */
  public JerseyConfigBuilder enableMoxyJsonFormattedOutput()
  {
    enableMoxyJsonFormattedOutput = true;
    enableMoxyXmlFeature();

    return this;
  }


  /**
   * Property for setting bean validation mode. Valid values BeanValidationMode.AUTO, BeanValidationMode.CALLBACK, BeanValidationMode.NONE (default). This will also {@link #enableMoxyXmlFeature() enableMoxyXmlFeature}.
   *
   * @param beanValidationMode
   *          Bean Validation Mode.
   * @return A self reference.
   */
  public JerseyConfigBuilder beanValidationMode(BeanValidationMode beanValidationMode)
  {
    this.beanValidationMode = beanValidationMode;
    enableMoxyXmlFeature();

    return this;
  }


  /**
   * A Bean Validation (JSR-349) support customization property.
   *
   * <p>
   * If set to {@code true} and Bean Validation support has not been explicitly disabled (see {@code BV_FEATURE_DISABLE}), the validation error information will be sent in the entity of the returned {@code javax.ws.rs.core.Response}.
   * </p>
   * <p>
   * The default value is {@code false}. This means that in case of an error response caused by a Bean Validation error, only a status code is sent in the server {@code Response} by default.
   * </p>
   *
   * @return A self reference.
   */
  public JerseyConfigBuilder enableBeanValidationSendErrorInResponse()
  {
    enableBeanValidationSendErrorInResponse = true;

    return this;
  }


  /**
   * Disable WADL generation.
   *
   * By default WADL generation is automatically enabled, if JAXB is present in the classpath.
   *
   * @return A self reference.
   */
  public JerseyConfigBuilder disableWadlFeature()
  {
    disableWadlFeature = true;

    return this;
  }


  /**
   * Add enabled configuration to supplied Server Config.
   *
   * @param serverConfig
   *          The resource configuration for configuring a web application that needs extra configuration.
   * @return Updated ResourceConfig.
   */
  public ResourceConfig configureServer(ResourceConfig serverConfig)
  {
    optionallyActivateXmlSupport(serverConfig);
    optionallyActivateLogTrafficDumpEntityWorkaround(serverConfig);
    optionallyActivateLogTrafficDumpEntityAlternative(serverConfig);

    serverConfig.property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, enableBeanValidationSendErrorInResponse);
    serverConfig.property(ServerProperties.WADL_FEATURE_DISABLE, disableWadlFeature);

    return serverConfig;
  }


  /**
   * Add enabled configuration to supplied Client Config.
   *
   * @param clientConfig
   *          Jersey externalized implementation of client-side JAX-RS configurable contract that needs extra configuration.
   * @return Updated ClientConfig.
   */
  public ClientConfig configureClient(ClientConfig clientConfig)
  {
    optionallyActivateXmlSupport(clientConfig);
    optionallyActivateLogTrafficDumpEntityWorkaround(clientConfig);
    optionallyActivateLogTrafficDumpEntityAlternative(clientConfig);

    return clientConfig;
  }


  /**
   * Duplicate supplied Server Config to supplied Client Config.
   *
   * @param clientConfig
   *          Jersey externalized implementation of client-side JAX-RS configurable contract that needs extra configuration.
   * @param serverConfig
   *          The resource configuration for configuring a web application that supplies extra configuration.
   * @return Updated ClientConfig.
   */
  public ClientConfig configureClientBasedOnServer(ClientConfig clientConfig, ResourceConfig serverConfig)
  {
    serverConfig.getInstances().forEach(clientConfig::register);

    return clientConfig;
  }


  private void optionallyActivateLogTrafficDumpEntityWorkaround(ResourceConfig serverConfig)
  {
    if (enableLogTrafficDumpEntityLoggingFeatureWorkaround)
    {
      serverConfig.register(new LoggingFeature(createFeatureLogger(), Level.INFO, LoggingFeature.Verbosity.PAYLOAD_TEXT, null));
    }
  }


  private void optionallyActivateLogTrafficDumpEntityAlternative(ResourceConfig serverConfig)
  {
    if (enableLogTrafficDumpEntityLoggingFeatureAlternative)
    {
      if (enableSlf4jBridge)
      {
        log.warn(LOGGING_FEATURE_ALTERNATIVE_WARN);
      }

      serverConfig.register(new LoggingFeature(createSlf4jFeatureLogger(), Level.OFF, LoggingFeature.Verbosity.PAYLOAD_TEXT, null));
    }
  }


  private void optionallyActivateLogTrafficDumpEntityWorkaround(ClientConfig clientConfig)
  {
    if (enableLogTrafficDumpEntityLoggingFeatureWorkaround)
    {
      clientConfig.register(new LoggingFeature(createFeatureLogger(), Level.INFO, LoggingFeature.Verbosity.PAYLOAD_TEXT, null));
    }
  }


  private void optionallyActivateLogTrafficDumpEntityAlternative(ClientConfig clientConfig)
  {
    if (enableLogTrafficDumpEntityLoggingFeatureAlternative)
    {
      if (enableSlf4jBridge)
      {
        log.warn(LOGGING_FEATURE_ALTERNATIVE_WARN);
      }

      clientConfig.register(new LoggingFeature(createSlf4jFeatureLogger(), Level.OFF, LoggingFeature.Verbosity.PAYLOAD_TEXT, null));
    }
  }


  private static Logger createFeatureLogger()
  {
    return Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME);
  }


  private Logger createSlf4jFeatureLogger()
  {
    return new Slf4jLoggingFeatureLevelWrapper(LoggingFeature.DEFAULT_LOGGER_NAME, null, logTrafficDumpEntityLoggingLevel);
  }


  private void optionallyActivateXmlSupport(ResourceConfig serverConfig)
  {
    if (enableMoxyXmlFeature)
    {
      serverConfig.register(new MoxyXmlFeature());
      serverConfig.register(createMoxyJsonResolver());

      configSwaggerCustomSpecFilter();
    }
  }


  private void optionallyActivateXmlSupport(ClientConfig clientConfig)
  {
    if (enableMoxyXmlFeature)
    {
      clientConfig.register(new MoxyXmlFeature());
      clientConfig.register(createMoxyJsonResolver());
    }
  }


  private ContextResolver<MoxyJsonConfig> createMoxyJsonResolver()
  {
    final MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();

    Map<String, String> namespacePrefixMapper = new HashMap<>(1);
    namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");

    moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper).setNamespaceSeparator(XMLConstants.COLON);
    moxyJsonConfig.setIncludeRoot(enableMoxyJsonIncludeRoot);
    moxyJsonConfig.setFormattedOutput(enableMoxyJsonFormattedOutput);

    moxyJsonConfig.property(MarshallerProperties.BEAN_VALIDATION_MODE, beanValidationMode);

    return moxyJsonConfig.resolver();
  }


  private void configSwaggerCustomSpecFilter()
  {
    if (enableMoxyJsonIncludeRoot)
    {
      CustomSpecFilter.enableIncludeRootSpecFilter();
    }
    else
    {
      CustomSpecFilter.disableIncludeRootSpecFilter();
    }
  }
}
