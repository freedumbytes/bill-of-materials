package nl.demon.shadowland.freedumbytes.rest.param;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.ext.ParamConverter;


/**
 * LocalDateTime ISO date-time format {@code ParamConverter} of a message parameter value and the corresponding custom Java type.
 */
public class LocalDateTimeConverter implements ParamConverter<LocalDateTime>
{
  @Override
  public LocalDateTime fromString(String value)
  {
    if (value == null || value.trim().isEmpty())
    {
      return null;
    }
    else
    {
      return LocalDateTime.parse(value);
    }
  }


  @Override
  public String toString(LocalDateTime value)
  {
    if (value == null)
    {
      return null;
    }
    else
    {
      return value.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
  }
}
