package nl.demon.shadowland.freedumbytes.rest.param;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.ext.ParamConverter;


/**
 * LocalDate ISO date format {@code ParamConverter} of a message parameter value and the corresponding custom Java type.
 */
public class LocalDateConverter implements ParamConverter<LocalDate>
{
  @Override
  public LocalDate fromString(String value)
  {
    if (value == null || value.trim().isEmpty())
    {
      return null;
    }
    else
    {
      return LocalDate.parse(value);
    }
  }


  @Override
  public String toString(LocalDate value)
  {
    if (value == null)
    {
      return null;
    }
    else
    {
      return value.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
  }
}
