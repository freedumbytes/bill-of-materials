package nl.demon.shadowland.freedumbytes.rest.swagger.filter;


import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.swagger.v3.core.filter.AbstractSpecFilter;
import io.swagger.v3.oas.models.OpenAPI;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class CustomSpecFilter extends AbstractSpecFilter
{
  private static boolean enableIncludeRootSpecFilter;

  private IncludeRootSpecFilter includeRootSpecFilter = new IncludeRootSpecFilter();


  @Override
  public Optional<OpenAPI> filterOpenAPI(OpenAPI openAPI, Map<String, List<String>> params, Map<String, String> cookies, Map<String, List<String>> headers)
  {
    if (isEnabledIncludeRootSpecFilter())
    {
      includeRootSpecFilter.filterOpenAPI(openAPI, params, cookies, headers);
    }

    return Optional.of(openAPI);
  }


  public static void enableIncludeRootSpecFilter()
  {
    enableIncludeRootSpecFilter = true;
  }


  public static void disableIncludeRootSpecFilter()
  {
    enableIncludeRootSpecFilter = false;
  }


  public static boolean isEnabledIncludeRootSpecFilter()
  {
    log.info("isEnabledIncludeRootSpecFilter:" + enableIncludeRootSpecFilter);

    return enableIncludeRootSpecFilter;
  }
}
