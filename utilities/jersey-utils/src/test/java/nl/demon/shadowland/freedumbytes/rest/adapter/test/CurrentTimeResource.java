package nl.demon.shadowland.freedumbytes.rest.adapter.test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


//@formatter:off
/**
 * What's the time? It's time to get ill.
 *
 * <pre>
 *    — Beastie Boys, Album Licensed to Ill
 * </pre>
 *
 * <p>
 * Issue 1 - Java 8 Date/Time API classes cause:
 * </p>
 *
 * <pre>
 * {@code
 *   JSON - java.lang.AssertionError: Expecting actual not to be null
 *            at nl.demon.shadowland.freedumbytes.rest.adapter.test.CurrentTimeResourceIT.currentLocalTime(CurrentTimeResourceIT.java:108)
 *
 *   XML  - java.lang.NoSuchMethodError: java.time.LocalTime.<init>()
 *            at com.sun.xml.internal.bind.v2.ClassFactory.create0(ClassFactory.java:96)
 *          Caused by: java.lang.NoSuchMethodException: java.time.LocalTime.<init>()
 *            at java.lang.Class.getConstructor0(Class.java:3082)
 *
 *   XML  - javax.ws.rs.client.ResponseProcessingException: Unexpected error during response processing.
 *            at org.glassfish.jersey.client.JerseyInvocation.translate(JerseyInvocation.java:880)
 *          Caused by: Exception [EclipseLink-3002] (Eclipse Persistence Services - 2.6.4): org.eclipse.persistence.exceptions.ConversionException
 *           Exception Description: The object [22:07:11.915], of class [class java.lang.String], from mapping [org.eclipse.persistence.oxm.mappings.XMLDirectMapping[localTime-->localTime/text()]]
 *           with descriptor [XMLDescriptor(nl.demon.shadowland.freedumbytes.rest.adapter.test.CurrentTime --> [DatabaseTable(currentTime)])], could not be converted to [class java.time.LocalTime].
 *             at org.eclipse.persistence.exceptions.ConversionException.couldNotBeConverted(ConversionException.java:78)}
 * </pre>
 *
 * <p>
 * Solution 1a - Property annotation {@code @XmlJavaTypeAdapter(LocalTimeAdapter.class)}.
 * </p>
 *
 * <p>
 * Solution 1b - Configure {@code package-info.java} in package folder with the following content:
 * </p>
 *
 * <pre>
 * &#64;XmlJavaTypeAdapters({
 *   &#64;XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateAdapter.class),
 *   &#64;XmlJavaTypeAdapter(type = LocalDateTime.class, value = LocalDateTimeAdapter.class),
 *   &#64;XmlJavaTypeAdapter(type = LocalTime.class, value = LocalTimeAdapter.class),
 * })
 * package nl.demon.shadowland.freedumbytes.rest.adapter.test;
 *
 * import java.time.LocalDate;
 * import java.time.LocalDateTime;
 * import java.time.LocalTime;
 *
 * import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
 * import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
 *
 * import nl.demon.shadowland.freedumbytes.rest.adapter.LocalDateAdapter;
 * import nl.demon.shadowland.freedumbytes.rest.adapter.LocalDateTimeAdapter;
 * import nl.demon.shadowland.freedumbytes.rest.adapter.LocalTimeAdapter;
 * </pre>
 *
 * <p>
 * Issue 2 - Default {@code com.sun.xml.internal.bind.v2.runtime.JAXBContextImpl} in combination with Lombok {@code @Getter} and {@code @XmlJavaTypeAdapter} causes:
 * </p>
 *
 * <pre>
 * {@code
 *   com.sun.xml.internal.bind.v2.runtime.IllegalAnnotationsException: 1 counts of IllegalAnnotationExceptions
 *   Class has two properties of the same name "localTime"
 *     this problem is related to the following location:
 *       at public java.time.LocalTime nl.demon.shadowland.freedumbytes.rest.adapter.test.CurrentTime.getLocalTime()
 *       at nl.demon.shadowland.freedumbytes.rest.adapter.test.CurrentTime
 *     this problem is related to the following location:
 *       at private java.time.LocalTime nl.demon.shadowland.freedumbytes.rest.adapter.test.CurrentTime.localTime
 *       at nl.demon.shadowland.freedumbytes.rest.adapter.test.CurrentTime}
 * </pre>
 *
 * <p>
 * Solution 2a - Class annotation {@code @XmlAccessorType(XmlAccessType.FIELD)}.
 * </p>
 *
 * <p>
 * Solution 2b - Switch to {@code org.eclipse.persistence.jaxb.JAXBContext} with {@code jaxb.properties} in package folder with the following content:
 * </p>
 *
 * <pre>
 * javax.xml.bind.context.factory = org.eclipse.persistence.jaxb.JAXBContextFactory
 * </pre>
 */
//@formatter:on
@Path("/timeToGetIll")
public class CurrentTimeResource
{
  @GET
  @Path("/currentLocalDate")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public CurrentTime currentLocalDate()
  {
    CurrentTime currentTime = new CurrentTime();
    currentTime.setLocalDate(LocalDate.now());

    return currentTime;
  }


  @GET
  @Path("/currentLocalDateTime")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public CurrentTime currentLocalDateTime()
  {
    CurrentTime currentTime = new CurrentTime();
    currentTime.setLocalDateTime(LocalDateTime.now());

    return currentTime;
  }


  @GET
  @Path("/currentLocalTime")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public CurrentTime currentLocalTime()
  {
    CurrentTime currentTime = new CurrentTime();
    currentTime.setLocalTime(LocalTime.now());

    return currentTime;
  }
}
