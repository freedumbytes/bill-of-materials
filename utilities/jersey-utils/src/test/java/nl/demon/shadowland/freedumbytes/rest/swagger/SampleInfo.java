package nl.demon.shadowland.freedumbytes.rest.swagger;


import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@XmlRootElement(name = "appInfo")
@XmlType(propOrder = { "name", "company", "version", "build", "time" })
@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
@Getter
@Setter
@ToString
@Tag(name = "demo")
@Schema(description = "Installed application information.")
public class SampleInfo
{
  @NonNull
  @Schema(description = "Application name.", example = "Sample Application")
  private String name;

  @NonNull
  @Schema(description = "Company name.", example = "Free Dumb Bytes aka freedumbytes")
  private String company;

  @NonNull
  @Schema(description = "Application version.", example = "1.0.0")
  private String version;

  @NonNull
  @Schema(description = "Application build date/time.", example = "2019-02-18 22:40:27 CET")
  private String build;

  @Schema(description = "Response date/time.", example = "2019-02-18T22:41:22.888")
  private LocalDateTime time = LocalDateTime.now();
}
