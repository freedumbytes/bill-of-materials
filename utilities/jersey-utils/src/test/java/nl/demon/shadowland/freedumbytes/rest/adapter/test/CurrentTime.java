package nl.demon.shadowland.freedumbytes.rest.adapter.test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import nl.demon.shadowland.freedumbytes.rest.adapter.LocalTimeAdapter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@XmlRootElement
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CurrentTime
{
  private LocalDate localDate;
  private LocalDateTime localDateTime;

  @XmlJavaTypeAdapter(LocalTimeAdapter.class)
  private LocalTime localTime;
}
