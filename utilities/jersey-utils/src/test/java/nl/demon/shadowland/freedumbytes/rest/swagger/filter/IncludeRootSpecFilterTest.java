package nl.demon.shadowland.freedumbytes.rest.swagger.filter;


import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;

import io.swagger.v3.core.filter.OpenAPISpecFilter;
import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.PathItem.HttpMethod;
import io.swagger.v3.oas.models.examples.Example;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.Discriminator;
import io.swagger.v3.oas.models.media.Encoding;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.XML;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;


public class IncludeRootSpecFilterTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(IncludeRootSpecFilter.class.getPackage().getName());

  private OpenAPISpecFilter filter = new IncludeRootSpecFilter();
  private OpenAPI openAPI;
  private PathItem pathItem;
  private Operation operation;
  private RequestBody requestBody;
  private ApiResponses apiResponses;
  private ApiResponse apiResponse;
  private Content requestBodyContent;
  private Content apiResponseContent;
  private MediaType xmlMediaType;
  private MediaType jsonMediaType;
  private Schema<Object> resusableInfoSchema;
  private Schema<Object> separateInfoSchema;
  private Components components;
  private Schema<Object> componentInfoSchema;
  private XML xml;


  @Test
  public void filterOpenAPI()
  {
    openAPI = new OpenAPI();

    verifyOpenAPI(false);

    pathItem = new PathItem();
    openAPI.path("/api/ping", pathItem);

    verifyPathItem(false);

    operation = new Operation();
    pathItem.operation(HttpMethod.GET, operation);

    verifyOperation(false);

    requestBody = new RequestBody();
    operation.requestBody(requestBody);

    apiResponses = new ApiResponses();
    operation.responses(apiResponses);

    verifyRequestBodyApiResponses(false);

    apiResponse = new ApiResponse();
    apiResponses.addApiResponse("default", apiResponse);

    verifyApiResponse(false);

    requestBodyContent = new Content();
    requestBody.content(requestBodyContent);

    apiResponseContent = new Content();
    apiResponse.content(apiResponseContent);

    verifyRequestBodyApiResponseContent(false);

    xmlMediaType = new MediaType();
    requestBodyContent.addMediaType("application/xml", xmlMediaType);
    apiResponseContent.addMediaType("application/xml", xmlMediaType);

    verifyXmlMediaType(false);

    resetApplicationJsonToReused();

    resusableInfoSchema = new Schema<>();
    resusableInfoSchema.$ref("#/components/schemas/Info");
    xmlMediaType.schema(resusableInfoSchema);

    components = new Components();
    componentInfoSchema = new Schema<>();
    components.addSchemas("Info", componentInfoSchema);
    openAPI.components(components);

    verifyJsonReusedXmlMediaType(false);

    jsonMediaType = new MediaType();
    resetApplicationJsonToSeparate();

    verifyJsonSeparateXmlMediaType(false);

    resetApplicationJsonToReused();

    componentInfoSchema.description("info description");

    xml = new XML();
    componentInfoSchema.xml(xml);

    verifyJsonReusedXmlMediaTypeRenamedDescription(false);

    resetApplicationJsonToSeparate();

    verifyJsonSeparateXmlMediaTypeRenamedDescription(false);

    resetApplicationJsonToReused();

    xml.name("tag");

    verifyJsonReusedXmlMediaTypePropTag(false);

    verifyReinvocation(false);

    resetApplicationJsonToSeparate();

    verifyJsonSeparateXmlMediaTypePropTag(false);

    loggerRule.verifyLoggingEventNever(Level.WARN);

    verifyCheckReusedAssumptionMediaType();
    verifyCheckReusedAssumptionMediaTypeSchema();
  }


  private void verifyCheckReusedAssumptionMediaType()
  {
    resetApplicationJsonToReused();

    Map<String, Example> examples = new HashMap<>();
    xmlMediaType.setExamples(examples);

    Map<String, Encoding> encodings = new HashMap<>();
    xmlMediaType.setEncoding(encodings);

    Map<String, Object> extensions = new HashMap<>();
    xmlMediaType.setExtensions(extensions);

    verifyJsonReusedXmlMediaTypeWarnings(0);

    resetApplicationJsonToReused();
    examples.put("key", new Example());
    verifyJsonReusedXmlMediaTypeWarnings(2);

    resetApplicationJsonToReused();
    examples.clear();
    xmlMediaType.setExample(new Object());
    verifyJsonReusedXmlMediaTypeWarnings(4);

    resetApplicationJsonToReused();
    xmlMediaType.setExample(null);
    encodings.put("key", new Encoding());
    verifyJsonReusedXmlMediaTypeWarnings(6);

    resetApplicationJsonToReused();
    encodings.clear();
    extensions.put("key", new Object());
    verifyJsonReusedXmlMediaTypeWarnings(8);

    resetApplicationJsonToReused();
    extensions.clear();
    verifyJsonReusedXmlMediaTypeWarnings(8);
  }


  private void verifyCheckReusedAssumptionMediaTypeSchema()
  {
    resetApplicationJsonToReused();

    List<String> required = new ArrayList<>();
    resusableInfoSchema.required(required);

    resusableInfoSchema.setProperties(new HashMap<>());

    Map<String, Object> extensions = new HashMap<>();
    resusableInfoSchema.setExtensions(extensions);

    List<Object> enums = new ArrayList<>();
    resusableInfoSchema.setEnum(enums);

    verifyJsonReusedXmlMediaTypeWarnings(8);

    resetApplicationJsonToReused();
    resusableInfoSchema.setDefault(new Object());
    verifyJsonReusedXmlMediaTypeWarnings(10);

    resetApplicationJsonToReused();
    resusableInfoSchema.setDefault(null);
    resusableInfoSchema.setName("name");
    verifyJsonReusedXmlMediaTypeWarnings(12);

    resetApplicationJsonToReused();
    resusableInfoSchema.setName(null);
    resusableInfoSchema.setTitle("title");
    verifyJsonReusedXmlMediaTypeWarnings(14);

    resetApplicationJsonToReused();
    resusableInfoSchema.setTitle(null);
    resusableInfoSchema.setMultipleOf(BigDecimal.ZERO);
    verifyJsonReusedXmlMediaTypeWarnings(16);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMultipleOf(null);
    resusableInfoSchema.setMaximum(BigDecimal.ZERO);
    verifyJsonReusedXmlMediaTypeWarnings(18);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMaximum(null);
    resusableInfoSchema.setExclusiveMaximum(true);
    verifyJsonReusedXmlMediaTypeWarnings(20);

    resetApplicationJsonToReused();
    resusableInfoSchema.setExclusiveMaximum(null);
    resusableInfoSchema.setMinimum(BigDecimal.ZERO);
    verifyJsonReusedXmlMediaTypeWarnings(22);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMinimum(null);
    resusableInfoSchema.setExclusiveMinimum(false);
    verifyJsonReusedXmlMediaTypeWarnings(24);

    resetApplicationJsonToReused();
    resusableInfoSchema.setExclusiveMinimum(null);
    resusableInfoSchema.setMaxLength(Integer.MAX_VALUE);
    verifyJsonReusedXmlMediaTypeWarnings(26);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMaxLength(null);
    resusableInfoSchema.setMinLength(Integer.MIN_VALUE);
    verifyJsonReusedXmlMediaTypeWarnings(28);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMinLength(null);
    resusableInfoSchema.setPattern("pattern");
    verifyJsonReusedXmlMediaTypeWarnings(30);

    resetApplicationJsonToReused();
    resusableInfoSchema.setPattern(null);
    resusableInfoSchema.setMaxItems(Integer.MAX_VALUE);
    verifyJsonReusedXmlMediaTypeWarnings(32);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMaxItems(null);
    resusableInfoSchema.setMinItems(Integer.MIN_VALUE);
    verifyJsonReusedXmlMediaTypeWarnings(34);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMinItems(null);
    resusableInfoSchema.setUniqueItems(true);
    verifyJsonReusedXmlMediaTypeWarnings(36);

    resetApplicationJsonToReused();
    resusableInfoSchema.setUniqueItems(null);
    resusableInfoSchema.setMaxProperties(Integer.MAX_VALUE);
    verifyJsonReusedXmlMediaTypeWarnings(38);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMaxProperties(null);
    resusableInfoSchema.setMinProperties(Integer.MIN_VALUE);
    verifyJsonReusedXmlMediaTypeWarnings(40);

    resetApplicationJsonToReused();
    resusableInfoSchema.setMinProperties(null);
    required.add("required");
    verifyJsonReusedXmlMediaTypeWarnings(42);

    resetApplicationJsonToReused();
    required.clear();
    resusableInfoSchema.setType("type");
    verifyJsonReusedXmlMediaTypeWarnings(44);

    resetApplicationJsonToReused();
    resusableInfoSchema.setType(null);
    resusableInfoSchema.setNot(new Schema<Object>());
    verifyJsonReusedXmlMediaTypeWarnings(46);

    resetApplicationJsonToReused();
    resusableInfoSchema.setNot(null);
    resusableInfoSchema.getProperties().put("key", new Schema<Object>());
    verifyJsonReusedXmlMediaTypeWarnings(48);

    resetApplicationJsonToReused();
    resusableInfoSchema.getProperties().clear();
    resusableInfoSchema.setAdditionalProperties(Boolean.TRUE);
    verifyJsonReusedXmlMediaTypeWarnings(50);

    resetApplicationJsonToReused();
    resusableInfoSchema.setAdditionalProperties(null);
    resusableInfoSchema.setDescription("description");
    verifyJsonReusedXmlMediaTypeWarnings(52);

    resetApplicationJsonToReused();
    resusableInfoSchema.setDescription(null);
    resusableInfoSchema.setFormat("format");
    verifyJsonReusedXmlMediaTypeWarnings(54);

    resetApplicationJsonToReused();
    resusableInfoSchema.setFormat(null);
    resusableInfoSchema.setNullable(true);
    verifyJsonReusedXmlMediaTypeWarnings(56);

    resetApplicationJsonToReused();
    resusableInfoSchema.setNullable(null);
    resusableInfoSchema.setReadOnly(false);
    verifyJsonReusedXmlMediaTypeWarnings(58);

    resetApplicationJsonToReused();
    resusableInfoSchema.setReadOnly(null);
    resusableInfoSchema.setWriteOnly(true);
    verifyJsonReusedXmlMediaTypeWarnings(60);

    resetApplicationJsonToReused();
    resusableInfoSchema.setWriteOnly(null);
    resusableInfoSchema.setExample(new Object());
    verifyJsonReusedXmlMediaTypeWarnings(62);

    resetApplicationJsonToReused();
    resusableInfoSchema.setExample(null);
    resusableInfoSchema.setExternalDocs(new ExternalDocumentation());
    verifyJsonReusedXmlMediaTypeWarnings(64);

    resetApplicationJsonToReused();
    resusableInfoSchema.setExternalDocs(null);
    resusableInfoSchema.setDeprecated(true);
    verifyJsonReusedXmlMediaTypeWarnings(66);

    resetApplicationJsonToReused();
    resusableInfoSchema.setDeprecated(null);
    resusableInfoSchema.setXml(new XML());
    verifyJsonReusedXmlMediaTypeWarnings(68);

    resetApplicationJsonToReused();
    resusableInfoSchema.setXml(null);
    extensions.put("key", new Object());
    verifyJsonReusedXmlMediaTypeWarnings(70);

    resetApplicationJsonToReused();
    extensions.clear();
    enums.add(new Object());
    verifyJsonReusedXmlMediaTypeWarnings(72);

    resetApplicationJsonToReused();
    enums.clear();
    resusableInfoSchema.setDiscriminator(new Discriminator());
    verifyJsonReusedXmlMediaTypeWarnings(74);

    resetApplicationJsonToReused();
    resusableInfoSchema.setDiscriminator(null);
    verifyJsonReusedXmlMediaTypeWarnings(74);
  }


  private void resetApplicationJsonToReused()
  {
    requestBodyContent.addMediaType("application/json", xmlMediaType);
    apiResponseContent.addMediaType("application/json", xmlMediaType);

    if (components != null)
    {
      components.getSchemas().remove("InfoIncludeRoot");
    }
  }


  private void resetApplicationJsonToSeparate()
  {
    requestBodyContent.addMediaType("application/json", jsonMediaType);
    apiResponseContent.addMediaType("application/json", jsonMediaType);

    separateInfoSchema = new Schema<>();
    separateInfoSchema.$ref("#/components/schemas/Info");
    jsonMediaType.schema(separateInfoSchema);

    components.getSchemas().remove("InfoIncludeRoot");
  }


  private Optional<OpenAPI> verifyOpenAPI(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = filter.filterOpenAPI(openAPI, null, null, null);

    assertThat(optionalOpenAPI.isPresent()).isTrue();

    if (!overlapOnly)
    {
      assertThat(openAPI.getPaths()).isNull();
      assertThat(openAPI.getComponents()).isNull();
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyPathItem(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyOpenAPI(true);

    assertThat(openAPI.getPaths()).isNotNull();
    assertThat(openAPI.getPaths()).hasSize(1);
    assertThat(openAPI.getPaths().get("/api/ping")).isEqualTo(pathItem);

    if (!overlapOnly)
    {
      assertThat(pathItem.readOperations()).isEmpty();
      assertThat(openAPI.getComponents()).isNull();
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyOperation(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyPathItem(true);

    assertThat(pathItem.readOperations()).hasSize(1);
    assertThat(pathItem.readOperations().get(0)).isEqualTo(operation);

    if (!overlapOnly)
    {
      assertThat(operation.getRequestBody()).isNull();
      assertThat(operation.getResponses()).isNull();
      assertThat(openAPI.getComponents()).isNull();
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyRequestBodyApiResponses(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyOperation(true);

    assertThat(operation.getRequestBody()).isEqualTo(requestBody);
    assertThat(operation.getResponses()).isEqualTo(apiResponses);

    if (!overlapOnly)
    {
      assertThat(operation.getResponses().values()).isEmpty();
      assertThat(requestBody.getContent()).isNull();
      assertThat(openAPI.getComponents()).isNull();
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyApiResponse(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyRequestBodyApiResponses(true);

    assertThat(operation.getResponses().entrySet()).hasSize(1);
    assertThat(operation.getResponses().values().iterator().next()).isEqualTo(apiResponse);

    if (!overlapOnly)
    {
      assertThat(requestBody.getContent()).isNull();
      assertThat(apiResponse.getContent()).isNull();
      assertThat(openAPI.getComponents()).isNull();
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyRequestBodyApiResponseContent(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyApiResponse(true);

    assertThat(requestBody.getContent()).isEqualTo(requestBodyContent);
    assertThat(apiResponse.getContent()).isEqualTo(apiResponseContent);

    if (!overlapOnly)
    {
      assertThat(requestBodyContent.entrySet()).isEmpty();
      assertThat(apiResponseContent.entrySet()).isEmpty();
      assertThat(openAPI.getComponents()).isNull();
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyXmlMediaType(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyRequestBodyApiResponseContent(true);

    if (!overlapOnly)
    {
      assertThat(requestBodyContent.entrySet()).hasSize(1);
      assertThat(apiResponseContent.entrySet()).hasSize(1);
    }

    assertThat(requestBodyContent.get("application/xml")).isEqualTo(xmlMediaType);
    assertThat(apiResponseContent.get("application/xml")).isEqualTo(xmlMediaType);

    if (!overlapOnly)
    {
      assertThat(xmlMediaType.getSchema()).isNull();
      assertThat(openAPI.getComponents()).isNull();
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyJsonReusedXmlMediaType(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyXmlMediaType(true);

    assertThat(requestBodyContent.entrySet()).hasSize(2);
    assertThat(requestBodyContent.get("application/json")).isNotNull();
    assertThat(apiResponseContent.entrySet()).hasSize(2);
    assertThat(apiResponseContent.get("application/json")).isNotNull();
    assertThat(xmlMediaType.getSchema()).isNotNull();
    assertThat(xmlMediaType.getSchema()).isEqualTo(resusableInfoSchema);
    assertThat(resusableInfoSchema.get$ref()).isEqualTo("#/components/schemas/Info");
    assertThat(requestBodyContent.get("application/json").getSchema()).isNotNull();
    assertThat(requestBodyContent.get("application/json").getSchema().get$ref()).isEqualTo("#/components/schemas/InfoIncludeRoot");
    assertThat(apiResponseContent.get("application/json").getSchema()).isNotNull();
    assertThat(apiResponseContent.get("application/json").getSchema().get$ref()).isEqualTo("#/components/schemas/InfoIncludeRoot");
    assertThat(openAPI.getComponents()).isNotNull();
    assertThat(openAPI.getComponents()).isEqualTo(components);
    assertThat(components.getSchemas()).hasSize(2);
    assertThat(components.getSchemas().get("Info")).isEqualTo(componentInfoSchema);
    assertThat(componentInfoSchema.getName()).isNull();

    if (!overlapOnly)
    {
      assertThat(componentInfoSchema.getDescription()).isNull();
      assertThat(componentInfoSchema.getXml()).isNull();
    }

    assertThat(components.getSchemas().get("InfoIncludeRoot")).isNotNull();

    if (!overlapOnly)
    {
      assertThat(components.getSchemas().get("InfoIncludeRoot").getDescription()).isNull();
    }

    assertThat(components.getSchemas().get("InfoIncludeRoot").getXml()).isNull();
    assertThat(components.getSchemas().get("InfoIncludeRoot").getProperties().size()).isEqualTo(1);

    if (!overlapOnly)
    {
      assertThat(components.getSchemas().get("InfoIncludeRoot").getProperties().get("info")).isNotNull();
      assertThat(components.getSchemas().get("InfoIncludeRoot").getProperties().get("info")).isInstanceOf(Schema.class);
      Schema<?> schema = (Schema<?>) components.getSchemas().get("InfoIncludeRoot").getProperties().get("info");
      assertThat(schema.get$ref()).isEqualTo("#/components/schemas/Info");
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyJsonSeparateXmlMediaType(boolean overlapOnly)
  {
    return verifyJsonReusedXmlMediaType(overlapOnly);
  }


  private Optional<OpenAPI> verifyJsonReusedXmlMediaTypeRenamedDescription(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyJsonReusedXmlMediaType(true);

    assertThat(componentInfoSchema.getDescription()).isEqualTo("info description");
    assertThat(componentInfoSchema.getXml()).isNotNull();

    if (!overlapOnly)
    {
      assertThat(componentInfoSchema.getXml().getName()).isNull();
    }

    assertThat(components.getSchemas().get("InfoIncludeRoot").getDescription()).isEqualTo("Include root workaround: info description");

    if (!overlapOnly)
    {
      assertThat(components.getSchemas().get("InfoIncludeRoot").getProperties().get("info")).isNotNull();
      assertThat(components.getSchemas().get("InfoIncludeRoot").getProperties().get("info")).isInstanceOf(Schema.class);
      Schema<?> schema = (Schema<?>) components.getSchemas().get("InfoIncludeRoot").getProperties().get("info");
      assertThat(schema.get$ref()).isEqualTo("#/components/schemas/Info");
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyJsonSeparateXmlMediaTypeRenamedDescription(boolean overlapOnly)
  {
    return verifyJsonReusedXmlMediaTypeRenamedDescription(overlapOnly);
  }


  private Optional<OpenAPI> verifyJsonReusedXmlMediaTypePropTag(boolean overlapOnly)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyJsonReusedXmlMediaTypeRenamedDescription(true);

    if (!overlapOnly)
    {
      assertThat(componentInfoSchema.getXml().getName()).isEqualTo("tag");
      assertThat(components.getSchemas().get("InfoIncludeRoot").getProperties().get("tag")).isNotNull();
      assertThat(components.getSchemas().get("InfoIncludeRoot").getProperties().get("tag")).isInstanceOf(Schema.class);
      Schema<?> schema = (Schema<?>) components.getSchemas().get("InfoIncludeRoot").getProperties().get("tag");
      assertThat(schema.get$ref()).isEqualTo("#/components/schemas/Info");
    }

    return optionalOpenAPI;
  }


  private Optional<OpenAPI> verifyReinvocation(boolean overlapOnly)
  {
    System.out.println(Yaml.pretty(openAPI));
    return verifyJsonReusedXmlMediaTypePropTag(overlapOnly);
  }


  private Optional<OpenAPI> verifyJsonSeparateXmlMediaTypePropTag(boolean overlapOnly)
  {
    return verifyJsonReusedXmlMediaTypePropTag(overlapOnly);
  }


  private Optional<OpenAPI> verifyJsonReusedXmlMediaTypeWarnings(int wantedNumberOfInvocations)
  {
    Optional<OpenAPI> optionalOpenAPI = verifyJsonReusedXmlMediaTypePropTag(false);

    List<LoggingEvent> warnings = loggerRule.verifyLoggingEvents(Level.WARN, wantedNumberOfInvocations);

    for (LoggingEvent loggingEvent : warnings)
    {
      assertThat(loggingEvent.getLevel()).isEqualTo(org.apache.log4j.Level.WARN);
      assertThat(loggingEvent.getMessage()).isEqualTo("MediaType reuse assumption is false; Do we need to implement clone MediaType?");
    }

    return optionalOpenAPI;
  }
}
