package nl.demon.shadowland.freedumbytes.rest.adapter;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

import org.junit.Test;


public class LocalTimeAdapterTest
{
  private static final String SAMPLE_TIME_TEXT = "23:58:59.999";
  private static final String INVALID_TIME_TEXT = "23:60:59.999";
  private static final LocalTime SAMPLE_LOCAL_TIME = LocalTime.of(23, 58, 59, 999000000);

  private final LocalTimeAdapter adapter = new LocalTimeAdapter();


  @Test
  public void convertStringNull()
  {
    assertThat(adapter.unmarshal(null)).isNull();
  }


  @Test
  public void convertStringBlank()
  {
    assertThat(adapter.unmarshal(" ")).isNull();
  }


  @Test
  public void convertStringValidTime()
  {
    assertThat(adapter.unmarshal(SAMPLE_TIME_TEXT)).isEqualTo(SAMPLE_LOCAL_TIME);
  }


  @Test(expected = DateTimeParseException.class)
  public void convertStringInvalidTime()
  {
    adapter.unmarshal(INVALID_TIME_TEXT);
  }


  @Test
  public void convertLocalTimeNull()
  {
    assertThat(adapter.marshal(null)).isNull();
  }


  @Test
  public void convertLocalTime()
  {
    assertThat(adapter.marshal(SAMPLE_LOCAL_TIME)).isEqualTo(SAMPLE_TIME_TEXT);
  }
}
