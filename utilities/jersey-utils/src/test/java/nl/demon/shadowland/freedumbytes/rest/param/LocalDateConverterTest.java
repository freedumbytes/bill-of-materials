package nl.demon.shadowland.freedumbytes.rest.param;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import org.junit.Test;


public class LocalDateConverterTest
{
  private static final String SAMPLE_DATE_TEXT = "2018-11-07";
  private static final String INVALID_DATE_TEXT = "2018-11-31";
  private static final LocalDate SAMPLE_LOCAL_DATE = LocalDate.of(2018, 11, 7);

  private final LocalDateConverter converter = new LocalDateConverter();


  @Test
  public void convertStringNull()
  {
    assertThat(converter.fromString(null)).isNull();
  }


  @Test
  public void convertStringBlank()
  {
    assertThat(converter.fromString(" ")).isNull();
  }


  @Test
  public void convertStringValidDate()
  {
    assertThat(converter.fromString(SAMPLE_DATE_TEXT)).isEqualTo(SAMPLE_LOCAL_DATE);
  }


  @Test(expected = DateTimeParseException.class)
  public void convertStringInvalidDate()
  {
    converter.fromString(INVALID_DATE_TEXT);
  }


  @Test
  public void convertLocalDateNull()
  {
    assertThat(converter.toString(null)).isNull();
  }


  @Test
  public void convertLocalDate()
  {
    assertThat(converter.toString(SAMPLE_LOCAL_DATE)).isEqualTo(SAMPLE_DATE_TEXT);
  }
}
