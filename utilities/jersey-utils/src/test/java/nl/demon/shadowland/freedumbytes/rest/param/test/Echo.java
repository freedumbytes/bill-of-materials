package nl.demon.shadowland.freedumbytes.rest.param.test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@XmlRootElement
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Echo
{
  private String text;
  private LocalDate localDate;
  private LocalDateTime localDateTime;
  private LocalTime localTime;
}
