package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;


@Provider
public class InterfaceParamConverterProvider implements ParamConverterProvider
{
  @Override
  @SuppressWarnings("unchecked")
  public <T> ParamConverter<T> getConverter(final Class<T> rawType, Type genericType, Annotation[] annotations)
  {
    if (ChildInterface.class.isAssignableFrom(rawType))
    {
      return (ParamConverter<T>) new ChildConverter();
    }
    if (rawType.isAssignableFrom(SiblingInterface.class))
    {
      return (ParamConverter<T>) new SiblingConverter();
    }
    else
    {
      return null;
    }
  }
}
