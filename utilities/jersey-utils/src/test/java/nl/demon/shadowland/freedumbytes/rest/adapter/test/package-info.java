@XmlJavaTypeAdapters({
  @XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateAdapter.class),
  @XmlJavaTypeAdapter(type = LocalDateTime.class, value = LocalDateTimeAdapter.class),
  @XmlJavaTypeAdapter(type = LocalTime.class, value = LocalTimeAdapter.class),
})
package nl.demon.shadowland.freedumbytes.rest.adapter.test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import nl.demon.shadowland.freedumbytes.rest.adapter.LocalDateAdapter;
import nl.demon.shadowland.freedumbytes.rest.adapter.LocalDateTimeAdapter;
import nl.demon.shadowland.freedumbytes.rest.adapter.LocalTimeAdapter;
