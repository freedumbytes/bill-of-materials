package nl.demon.shadowland.freedumbytes.rest.param;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

import org.junit.Test;


public class LocalTimeConverterTest
{
  private static final String SAMPLE_TIME_TEXT = "23:58:59.999";
  private static final String INVALID_TIME_TEXT = "23:60:59.999";
  private static final LocalTime SAMPLE_LOCAL_TIME = LocalTime.of(23, 58, 59, 999000000);

  private final LocalTimeConverter converter = new LocalTimeConverter();


  @Test
  public void convertStringNull()
  {
    assertThat(converter.fromString(null)).isNull();
  }


  @Test
  public void convertStringBlank()
  {
    assertThat(converter.fromString(" ")).isNull();
  }


  @Test
  public void convertStringValidTime()
  {
    assertThat(converter.fromString(SAMPLE_TIME_TEXT)).isEqualTo(SAMPLE_LOCAL_TIME);
  }


  @Test(expected = DateTimeParseException.class)
  public void convertStringInvalidTime()
  {
    converter.fromString(INVALID_TIME_TEXT);
  }


  @Test
  public void convertLocalTimeNull()
  {
    assertThat(converter.toString(null)).isNull();
  }


  @Test
  public void convertLocalTime()
  {
    assertThat(converter.toString(SAMPLE_LOCAL_TIME)).isEqualTo(SAMPLE_TIME_TEXT);
  }
}
