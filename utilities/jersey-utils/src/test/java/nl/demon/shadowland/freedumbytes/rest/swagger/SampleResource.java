package nl.demon.shadowland.freedumbytes.rest.swagger;


import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;


@Path("/api/v1")
@Tag(name = "demo")
@Singleton
@Component
@Slf4j
public class SampleResource
{
  private String applicationName = "Sample Swagger Annotation and The OpenAPI Specification.";
  private String company = "Free Dumb Bytes aka freedumbytes";
  private String version = "0.1.0";
  private String build = "2019-02-28 17:26:00 CET";


  @GET
  @Path("/sample")
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
  public SampleInfo sample()
  {
    return SampleInfo.of(applicationName, company, version, build);
  }


  @GET
  @Path("/sampleAnnotated")
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
//@formatter:off
  @Operation(
      summary = "Application health.",
      description = "Check application availability.",
      responses = {
          @ApiResponse(
              responseCode = "200",
              description = "Application information when it is up and running.",
              content = {
                  @Content(
                      mediaType = MediaType.APPLICATION_JSON,
                      schema = @Schema(
                          implementation = SampleInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XML,
                      schema = @Schema(
                          implementation = SampleInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XHTML_XML,
                      schema = @Schema(
                          implementation = SampleInfo.class
                          )
                      )
                  }
              ),
          @ApiResponse(
              responseCode = "404",
              description = "Application is down."
              )
          }
      )
//@formatter:on
  public SampleInfo sampleAnnotated()
  {
    return SampleInfo.of(applicationName, company, version, build);
  }


  @GET
  @Path("/sampleAnnotatedAndNamed")
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
//@formatter:off
  @Operation(
      summary = "Application health.",
      description = "Check application availability.",
      responses = {
          @ApiResponse(
              responseCode = "200",
              description = "Application information when it is up and running.",
              content = {
                  @Content(
                      mediaType = MediaType.APPLICATION_JSON,
                      schema = @Schema(
                          implementation = SampleNamedInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XML,
                      schema = @Schema(
                          implementation = SampleNamedInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XHTML_XML,
                      schema = @Schema(
                          implementation = SampleNamedInfo.class)
                      )
                  }
              ),
          @ApiResponse(
              responseCode = "404",
              description = "Application is down."
              )
          }
      )
//@formatter:on
  public SampleNamedInfo sampleAnnotatedAndNamed()
  {
    return SampleNamedInfo.of(applicationName, company, version, build);
  }


  @POST
  @Path("/updateSampleApp")
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
  public SampleInfo updateApplication(SampleApplication application)
  {
    log.info("Updating " + application);

    return SampleInfo.of(application.getName(), company, version, build);
  }


  @POST
  @Path("/updateSampleAppAnnotated")
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.APPLICATION_XHTML_XML })
//@formatter:off
  @Operation(
      summary = "Update an application",
      tags = {
          "demo",
          "apps"
          },
      responses = {
          @ApiResponse(
              responseCode = "200",
              description = "Update application with supplied information.",
              content = {
                  @Content(
                      mediaType = MediaType.APPLICATION_JSON,
                      schema = @Schema(
                          implementation = SampleInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XML,
                      schema = @Schema(
                          implementation = SampleInfo.class
                          )
                      ),
                  @Content(
                      mediaType = MediaType.APPLICATION_XHTML_XML,
                      schema = @Schema(
                          implementation = SampleInfo.class)
                      )
                  }
              ),
          @ApiResponse(
              responseCode = "400",
              description = "Invalid ID supplied."
              ),
          @ApiResponse(
              responseCode = "404",
              description = "Application not found."
              ),
          @ApiResponse(
              responseCode = "405",
              description = "Validation exception."
              )
          }
      )
//@formatter:on
  public SampleInfo updateApplicationAnnotated(@RequestBody(description = "Application that needs to be updated.", required = true, content = @Content(schema = @Schema(implementation = SampleApplication.class))) SampleApplication application)
  {
    log.info("Updating " + application);

    return SampleInfo.of(application.getName(), company, version, build);
  }
}
