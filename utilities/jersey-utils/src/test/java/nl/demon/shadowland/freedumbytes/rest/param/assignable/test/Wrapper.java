package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


@XmlRootElement
@Getter
@Setter
@Accessors(fluent = true)
@ToString
@EqualsAndHashCode
public class Wrapper
{
  @XmlElement
  private Parent parent;
}
