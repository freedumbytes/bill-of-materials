package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;


public class ChildConverterTest
{
  private static final String SAMPLE_CHILD_TEXT = "theChild";
  private static final Child SAMPLE_CHILD = new Child(SAMPLE_CHILD_TEXT).message(ChildConverter.KILROY_SIGNATURE);

  private final ChildConverter converter = new ChildConverter();


  @Test
  public void convertStringNull()
  {
    assertThat(converter.fromString(null)).isNull();
  }


  @Test
  public void convertStringBlank()
  {
    assertThat(converter.fromString(" ")).isNull();
  }


  @Test
  public void convertString()
  {
    Child child = converter.fromString(SAMPLE_CHILD_TEXT);

    assertThat(child.noIWasNot()).isFalse();
    assertThat(child).isEqualTo(SAMPLE_CHILD);
  }


  @Test
  public void convertChildNull()
  {
    assertThat(converter.toString(null)).isNull();
  }


  @Test
  public void convertChild()
  {
    assertThat(converter.toString(SAMPLE_CHILD)).isEqualTo(SAMPLE_CHILD_TEXT);
  }
}
