package nl.demon.shadowland.freedumbytes.rest.adapter;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import org.junit.Test;


public class LocalDateTimeAdapterTest
{
  private static final String SAMPLE_DATE_TIME_TEXT = "2018-11-07T23:58:59.999";
  private static final String INVALID_DATE_TIME_TEXT = "2018-11-07T23:60:59.999";
  private static final LocalDateTime SAMPLE_LOCAL_DATE_TIME = LocalDateTime.of(2018, 11, 7, 23, 58, 59, 999000000);

  private final LocalDateTimeAdapter adapter = new LocalDateTimeAdapter();


  @Test
  public void convertStringNull()
  {
    assertThat(adapter.unmarshal(null)).isNull();
  }


  @Test
  public void convertStringBlank()
  {
    assertThat(adapter.unmarshal(" ")).isNull();
  }


  @Test
  public void convertStringValidDateTime()
  {
    assertThat(adapter.unmarshal(SAMPLE_DATE_TIME_TEXT)).isEqualTo(SAMPLE_LOCAL_DATE_TIME);
  }


  @Test(expected = DateTimeParseException.class)
  public void convertStringInvalidDateTime()
  {
    adapter.unmarshal(INVALID_DATE_TIME_TEXT);
  }


  @Test
  public void convertLocalDateTimeNull()
  {
    assertThat(adapter.marshal(null)).isNull();
  }


  @Test
  public void convertLocalDateTime()
  {
    assertThat(adapter.marshal(SAMPLE_LOCAL_DATE_TIME)).isEqualTo(SAMPLE_DATE_TIME_TEXT);
  }
}
