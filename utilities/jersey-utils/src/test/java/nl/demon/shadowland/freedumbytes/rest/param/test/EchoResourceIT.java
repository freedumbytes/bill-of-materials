package nl.demon.shadowland.freedumbytes.rest.param.test;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.moxy.xml.MoxyXmlFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;

import nl.demon.shadowland.freedumbytes.rest.param.CustomParamConverterProvider;


public class EchoResourceIT extends JerseyTest
{
  @Test
  public void echoStringUndefinedJson()
  {
    echoStringUndefined(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void echoStringUndefinedXml()
  {
    echoStringUndefined(MediaType.APPLICATION_XML_TYPE);
  }


  private void echoStringUndefined(MediaType mediaType)
  {
    WebTarget target = target("echo/string");
    Echo echo = target.request(mediaType).get(Echo.class);

    assertThat(echo.getText()).isEqualTo("Undefined");
  }


  @Test
  public void echoStringJson()
  {
    echoString(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void echoStringXml()
  {
    echoString(MediaType.APPLICATION_XML_TYPE);
  }


  private void echoString(MediaType mediaType)
  {
    String text = "Testing 1 2 3";
    WebTarget target = target("echo/string").queryParam("echo", text);
    Echo echo = target.request(mediaType).get(Echo.class);

    assertThat(echo.getText()).isEqualTo(text);
  }


  @Test
  public void echoLocalDateJson()
  {
    echoLocalDate(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void echoLocalDateXml()
  {
    echoLocalDate(MediaType.APPLICATION_XML_TYPE);
  }


  private void echoLocalDate(MediaType mediaType)
  {
    LocalDate now = LocalDate.now();
    WebTarget target = target("echo/localDate").queryParam("echo", now);
    Echo echo = target.request(mediaType).get(Echo.class);

    assertThat(echo.getLocalDate()).isEqualTo(now);
  }


  @Test
  public void echoLocalDateTimeJson()
  {
    echoLocalDateTime(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void echoLocalDateTimeXml()
  {
    echoLocalDateTime(MediaType.APPLICATION_XML_TYPE);
  }


  private void echoLocalDateTime(MediaType mediaType)
  {
    LocalDateTime now = LocalDateTime.now();
    WebTarget target = target("echo/localDateTime").queryParam("echo", now);
    Echo echo = target.request(mediaType).get(Echo.class);

    assertThat(echo.getLocalDateTime()).isEqualTo(now);
  }


  @Test
  public void echoLocalTimeJson()
  {
    echoLocalTime(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void echoLocalTimeXml()
  {
    echoLocalTime(MediaType.APPLICATION_XML_TYPE);
  }


  private void echoLocalTime(MediaType mediaType)
  {
    LocalTime now = LocalTime.now();
    WebTarget target = target("echo/localTime").queryParam("echo", now);
    Echo echo = target.request(mediaType).get(Echo.class);

    assertThat(echo.getLocalTime()).isEqualTo(now);
  }


  @Test
  public void jaxbContext() throws JAXBException
  {
    assertThat(JAXBContext.newInstance(EchoResource.class).getClass().getName()).isEqualTo("com.sun.xml.internal.bind.v2.runtime.JAXBContextImpl");
    assertThat(JAXBContext.newInstance(Echo.class).getClass().getName()).isEqualTo("com.sun.xml.internal.bind.v2.runtime.JAXBContextImpl");
  }


  @Override
  protected Application configure()
  {
    ResourceConfig serverConfig = new ResourceConfig(EchoResource.class, CustomParamConverterProvider.class);

    activateXmlSupport(serverConfig);
    activateSlf4jBridge();
    activateLogTrafficDumpEntity(serverConfig);

    return serverConfig;
  }


  private void activateXmlSupport(ResourceConfig serverConfig)
  {
    serverConfig.register(new MoxyXmlFeature());
    serverConfig.register(moxyJsonResolver());
  }


  private ContextResolver<MoxyJsonConfig> moxyJsonResolver()
  {
    MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();

    Map<String, String> namespacePrefixMapper = new HashMap<>(1);
    namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");

    moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper).setNamespaceSeparator(':');
    moxyJsonConfig.setFormattedOutput(true);

    return moxyJsonConfig.resolver();
  }


  private void activateSlf4jBridge()
  {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
  }


  private void activateLogTrafficDumpEntity(ResourceConfig serverConfig)
  {
    Logger logger = Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME);
    serverConfig.register(new LoggingFeature(logger, Level.INFO, LoggingFeature.Verbosity.PAYLOAD_TEXT, null));
  }
}
