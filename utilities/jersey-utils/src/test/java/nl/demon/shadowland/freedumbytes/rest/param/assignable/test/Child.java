package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


@NoArgsConstructor
@Getter
@Setter
@Accessors(fluent = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Child extends Parent implements ChildInterface
{
  private String message;


  public Child(String value)
  {
    value(value);
  }


  public boolean noIWasNot()
  {
    return !ChildConverter.KILROY_SIGNATURE.equals(message);
  }
}
