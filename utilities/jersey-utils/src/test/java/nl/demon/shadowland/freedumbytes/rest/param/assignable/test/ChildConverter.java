package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import javax.ws.rs.ext.ParamConverter;


public class ChildConverter implements ParamConverter<Child>
{
  public static final String KILROY_SIGNATURE = "Kilroy was here";


  @Override
  public Child fromString(String value)
  {
    if (value == null || value.trim().isEmpty())
    {
      return null;
    }
    else
    {
      return new Child(value).message(KILROY_SIGNATURE);
    }
  }


  @Override
  public String toString(Child value)
  {
    if (value == null)
    {
      return null;
    }
    else
    {
      return value.value();
    }
  }
}
