package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Singleton;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;


/**
 * That's a wrap.
 *
 * <p>
 * Demonstrate the (in)correct usage/order of {@code class.isAssignableFrom} in {@link InterfaceParamConverterProvider}.
 * </p>
 * <p>
 * The {@code @QueryParam("theChild")} at {@code @Path("/correctAssignableFrom")} is created by {@link ChildConverter} because the {@code message} is also set.
 * </p>
 * <p>
 * The {@code @QueryParam("theSibling")} at {@code @Path("/incorrectAssignableFrom")} isn't created by {@link SiblingConverter} because the {@code message} is blank.
 * </p>
 * <p>
 * By the way notice that the server side {@code Wrapper} object contains a {@code Child} or {@code Sibling} and the client will unmarshal the JSON value as {@code Parent} property of {@code Wrapper} (see {@code isExactlyInstanceOf(...)} calls below and in
 * {@link InterfaceResourceIT}).
 * </p>
 * <p>
 * When using Lombok {@code @Accessors(fluent = true)} all properties must be annotated with {@code @XmlElement} or the class must be annotated with {@code @XmlAccessorType(XmlAccessType.FIELD)}.
 * </p>
 */
@Path("/wrap")
@Singleton
@Slf4j
public class InterfaceResource
{
  @GET
  @Path("/correctAssignableFrom")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Wrapper wrap(@DefaultValue("Unknown") @QueryParam("theChild") Child child)
  {
    Wrapper wrapper = new Wrapper().parent(child);
    log.info("{}", child);
    log.info("{}", wrapper);

    try
    {
      assertThat(child.message()).isEqualTo(ChildConverter.KILROY_SIGNATURE);

      assertThat(wrapper.parent()).isInstanceOf(Parent.class);
      assertThat(wrapper.parent()).isInstanceOf(Child.class);
      assertThat(wrapper.parent()).isExactlyInstanceOf(Child.class);
    }
    catch (AssertionError e)
    {
      log.error("Missing graffiti.", e);

      throw e;
    }

    return wrapper;
  }


  @GET
  @Path("/incorrectAssignableFrom")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Wrapper wrap(@DefaultValue("Unknown") @QueryParam("theSibling") Sibling sibling)
  {
    Wrapper wrapper = new Wrapper().parent(sibling);
    log.info("{}", sibling);
    log.info("{}", wrapper);

    try
    {
      assertThat(sibling.message()).isNotEqualTo(ChildConverter.KILROY_SIGNATURE);
      assertThat(sibling.message()).isNull();
      assertThat(sibling.value()).isEqualTo("aSibling");

      sibling.value(sibling.value() + " - message is empty; thus SiblingConverter wasn't used.");

      assertThat(wrapper.parent()).isInstanceOf(Parent.class);
      assertThat(wrapper.parent()).isInstanceOf(Sibling.class);
      assertThat(wrapper.parent()).isExactlyInstanceOf(Sibling.class);
    }
    catch (AssertionError e)
    {
      log.error("Found graffiti.", e);

      throw e;
    }

    return wrapper;
  }
}
