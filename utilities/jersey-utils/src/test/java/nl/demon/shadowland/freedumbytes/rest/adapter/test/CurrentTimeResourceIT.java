package nl.demon.shadowland.freedumbytes.rest.adapter.test;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.moxy.xml.MoxyXmlFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;


public class CurrentTimeResourceIT extends JerseyTest
{
  @Test
  public void currentLocalDateJson()
  {
    currentLocalDate(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void currentLocalDateXml()
  {
    currentLocalDate(MediaType.APPLICATION_XML_TYPE);
  }


  private void currentLocalDate(MediaType mediaType)
  {
    LocalDate now = LocalDate.now();
    WebTarget target = target("timeToGetIll/currentLocalDate");
    CurrentTime currentTime = target.request(mediaType).get(CurrentTime.class);

    assertThat(currentTime.getLocalDate()).isAfterOrEqualTo(now);
  }


  @Test
  public void currentLocalDateTimeJson()
  {
    currentLocalDateTime(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void currentLocalDateTimeXml()
  {
    currentLocalDateTime(MediaType.APPLICATION_XML_TYPE);
  }


  private void currentLocalDateTime(MediaType mediaType)
  {
    LocalDateTime now = LocalDateTime.now();
    WebTarget target = target("timeToGetIll/currentLocalDateTime");
    CurrentTime currentTime = target.request(mediaType).get(CurrentTime.class);

    assertThat(currentTime.getLocalDateTime()).isAfterOrEqualTo(now);
  }


  @Test
  public void currentLocalTimeJson()
  {
    currentLocalTime(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void currentLocalTimeXml()
  {
    currentLocalTime(MediaType.APPLICATION_XML_TYPE);
  }


  private void currentLocalTime(MediaType mediaType)
  {
    LocalTime now = LocalTime.now();
    WebTarget target = target("timeToGetIll/currentLocalTime");
    CurrentTime currentTime = target.request(mediaType).get(CurrentTime.class);

    assertThat(currentTime.getLocalTime()).isAfterOrEqualTo(now);
  }


  @Test
  public void jaxbContext() throws JAXBException
  {
    assertThat(JAXBContext.newInstance(CurrentTimeResource.class).getClass().getName()).isEqualTo("org.eclipse.persistence.jaxb.JAXBContext");
    assertThat(JAXBContext.newInstance(CurrentTime.class).getClass().getName()).isEqualTo("org.eclipse.persistence.jaxb.JAXBContext");
  }


  @Override
  protected Application configure()
  {
    ResourceConfig serverConfig = new ResourceConfig(CurrentTimeResource.class);

    activateXmlSupport(serverConfig);
    activateSlf4jBridge();
    activateLogTrafficDumpEntity(serverConfig);

    return serverConfig;
  }


  private void activateXmlSupport(ResourceConfig serverConfig)
  {
    serverConfig.register(new MoxyXmlFeature());
    serverConfig.register(moxyJsonResolver());
  }


  private ContextResolver<MoxyJsonConfig> moxyJsonResolver()
  {
    MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();

    Map<String, String> namespacePrefixMapper = new HashMap<>(1);
    namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");

    moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper).setNamespaceSeparator(':');
    moxyJsonConfig.setFormattedOutput(true);

    return moxyJsonConfig.resolver();
  }


  private void activateSlf4jBridge()
  {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
  }


  private void activateLogTrafficDumpEntity(ResourceConfig serverConfig)
  {
    Logger logger = Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME);
    serverConfig.register(new LoggingFeature(logger, Level.INFO, LoggingFeature.Verbosity.PAYLOAD_TEXT, null));
  }
}
