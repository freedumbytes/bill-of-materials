package nl.demon.shadowland.freedumbytes.rest.adapter;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import org.junit.Test;


public class LocalDateAdapterTest
{
  private static final String SAMPLE_DATE_TEXT = "2018-11-07";
  private static final String INVALID_DATE_TEXT = "2018-11-31";
  private static final LocalDate SAMPLE_LOCAL_DATE = LocalDate.of(2018, 11, 7);

  private final LocalDateAdapter adapter = new LocalDateAdapter();


  @Test
  public void convertStringNull()
  {
    assertThat(adapter.unmarshal(null)).isNull();
  }


  @Test
  public void convertStringBlank()
  {
    assertThat(adapter.unmarshal(" ")).isNull();
  }


  @Test
  public void convertStringValidDate()
  {
    assertThat(adapter.unmarshal(SAMPLE_DATE_TEXT)).isEqualTo(SAMPLE_LOCAL_DATE);
  }


  @Test(expected = DateTimeParseException.class)
  public void convertStringInvalidDate()
  {
    adapter.unmarshal(INVALID_DATE_TEXT);
  }


  @Test
  public void convertLocalDateNull()
  {
    assertThat(adapter.marshal(null)).isNull();
  }


  @Test
  public void convertLocalDate()
  {
    assertThat(adapter.marshal(SAMPLE_LOCAL_DATE)).isEqualTo(SAMPLE_DATE_TEXT);
  }
}
