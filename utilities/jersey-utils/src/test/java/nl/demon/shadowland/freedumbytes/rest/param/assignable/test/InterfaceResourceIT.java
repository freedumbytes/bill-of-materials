package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.moxy.xml.MoxyXmlFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;


public class InterfaceResourceIT extends JerseyTest
{
  @Test
  public void wrapCorrectAssignableFromUnknownJson()
  {
    wrapCorrectAssignableFromUnknown(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void wrapCorrectAssignableFromUnknownXml()
  {
    wrapCorrectAssignableFromUnknown(MediaType.APPLICATION_XML_TYPE);
  }


  private void wrapCorrectAssignableFromUnknown(MediaType mediaType)
  {
    WebTarget target = target("wrap/correctAssignableFrom");
    Wrapper wrapper = target.request(MediaType.APPLICATION_JSON_TYPE).get(Wrapper.class);

    assertThat(wrapper.parent().value()).isEqualTo("Unknown");

    assertThat(wrapper.parent()).isInstanceOf(Parent.class);
    assertThat(wrapper.parent()).isNotInstanceOf(Child.class);
    assertThat(wrapper.parent()).isExactlyInstanceOf(Parent.class);
  }


  @Test
  public void wrapCorrectAssignableFromJson()
  {
    wrapCorrectAssignableFrom(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void wrapCorrectAssignableFromXml()
  {
    wrapCorrectAssignableFrom(MediaType.APPLICATION_XML_TYPE);
  }


  private void wrapCorrectAssignableFrom(MediaType mediaType)
  {
    WebTarget target = target("wrap/correctAssignableFrom").queryParam("theChild", "aChild");
    Wrapper wrapper = target.request(MediaType.APPLICATION_JSON_TYPE).get(Wrapper.class);

    assertThat(wrapper.parent().value()).isEqualTo("aChild");

    assertThat(wrapper.parent()).isInstanceOf(Parent.class);
    assertThat(wrapper.parent()).isNotInstanceOf(Child.class);
    assertThat(wrapper.parent()).isExactlyInstanceOf(Parent.class);
  }


  @Test
  public void wrapIncorrectAssignableFromJson()
  {
    wrapIncorrectAssignableFrom(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void wrapIncorrectAssignableFromXml()
  {
    wrapIncorrectAssignableFrom(MediaType.APPLICATION_XML_TYPE);
  }


  private void wrapIncorrectAssignableFrom(MediaType mediaType)
  {
    WebTarget target = target("wrap/incorrectAssignableFrom").queryParam("theSibling", "aSibling");
    Wrapper wrapper = target.request(MediaType.APPLICATION_JSON_TYPE).get(Wrapper.class);

    assertThat(wrapper.parent().value()).isEqualTo("aSibling - message is empty; thus SiblingConverter wasn't used.");

    assertThat(wrapper.parent()).isInstanceOf(Parent.class);
    assertThat(wrapper.parent()).isNotInstanceOf(Sibling.class);
    assertThat(wrapper.parent()).isExactlyInstanceOf(Parent.class);
  }


  @Override
  protected Application configure()
  {
    ResourceConfig serverConfig = new ResourceConfig(InterfaceResource.class, InterfaceParamConverterProvider.class);

    activateXmlSupport(serverConfig);
    activateSlf4jBridge();
    activateLogTrafficDumpEntity(serverConfig);

    return serverConfig;
  }


  private void activateXmlSupport(ResourceConfig serverConfig)
  {
    serverConfig.register(new MoxyXmlFeature());
    serverConfig.register(moxyJsonResolver());
  }


  private ContextResolver<MoxyJsonConfig> moxyJsonResolver()
  {
    MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();

    Map<String, String> namespacePrefixMapper = new HashMap<>(1);
    namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");

    moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper).setNamespaceSeparator(':');
    moxyJsonConfig.setFormattedOutput(true);

    return moxyJsonConfig.resolver();
  }


  private void activateSlf4jBridge()
  {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
  }


  private void activateLogTrafficDumpEntity(ResourceConfig serverConfig)
  {
    Logger logger = Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME);
    serverConfig.register(new LoggingFeature(logger, Level.INFO, LoggingFeature.Verbosity.PAYLOAD_TEXT, null));
  }
}
