package nl.demon.shadowland.freedumbytes.rest.config;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import org.apache.log4j.Level;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;


public class JerseyConfigBuilderSlf4jLogManagerTest extends JerseyConfigBuilderTest
{
  @Test
  public void doesNotGloballyEnableSlf4jBridge()
  {
    assertThat(SLF4JBridgeHandler.isInstalled()).isFalse();

    JerseyConfigBuilder.globallyEnableSlf4jBridge();

    assertThat(SLF4JBridgeHandler.isInstalled()).isFalse();
  }


  @Test
  public void enableLogTrafficDumpEntityLoggingFeatureAlternativeWarn()
  {
    JerseyConfigBuilder.globallyEnableSlf4jBridge();

    JerseyConfigBuilder configBuilder = new JerseyConfigBuilder();
    ResourceConfig serverConfig = new ResourceConfig();
    ClientConfig clientConfig = new ClientConfig();

    configBuilder.configureServer(serverConfig);
    configBuilder.configureClient(clientConfig);

    assertThat(serverConfig.getInstances()).isEmpty();
    assertThat(clientConfig.getInstances()).isEmpty();

    configBuilder.enableLogTrafficDumpEntityLoggingFeatureAlternative();

    configBuilder.configureServer(serverConfig);
    configBuilder.configureClient(clientConfig);

    assertThat(serverConfig.getInstances()).hasSize(1);
    assertThat(clientConfig.getInstances()).hasSize(1);

    assertThat(serverConfig.getInstances().toArray()[0]).isInstanceOf(LoggingFeature.class);
    assertThat(clientConfig.getInstances().toArray()[0]).isInstanceOf(LoggingFeature.class);

    loggerRule.verifyLoggingEvents(times(1));
    loggerRule.verifyLoggingEvents(Level.INFO, 1);
    loggerRule.verifyLoggingEvents(Level.WARN, 0);
  }
}
