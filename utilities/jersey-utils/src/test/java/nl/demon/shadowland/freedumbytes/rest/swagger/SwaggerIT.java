package nl.demon.shadowland.freedumbytes.rest.swagger;


import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.rest.adapter.test.CurrentTimeResource;
import nl.demon.shadowland.freedumbytes.rest.config.JerseyConfigBuilder;
import nl.demon.shadowland.freedumbytes.rest.param.CustomParamConverterProvider;
import nl.demon.shadowland.freedumbytes.rest.param.assignable.test.InterfaceResource;
import nl.demon.shadowland.freedumbytes.rest.param.test.EchoResource;
import nl.demon.shadowland.freedumbytes.rest.swagger.filter.CustomSpecFilter;

import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;


public class SwaggerIT extends JerseyTest
{
  public final static MediaType APPLICATION_YAML_TYPE = new MediaType("application", "yaml");

  private static final String REQUEST_COMPONENT_NAME = "SampleApplication";
  private static final String[] COMPONENT_NAMES = new String[] { "CurrentTime", "Wrapper", "Echo", "SampleInfo", "sampleNamed", REQUEST_COMPONENT_NAME };
  private static final String COMPONENT_NAME_VAR = "<ComponentName>";

  private static final String YAML_COMPONENTS_ENTRIES = "components:";
  private static final String YAML_REQUEST_PATH_SCHEMA_REF_TEMPLATE = "              $ref: '#/components/schemas/" + COMPONENT_NAME_VAR + "'";
  private static final String YAML_REQUEST_PATH_SCHEMA_REF_INCLUDE_ROOT_TEMPLATE = "              $ref: '#/components/schemas/" + COMPONENT_NAME_VAR + "IncludeRoot'";
  private static final String YAML_RESPONSE_PATH_SCHEMA_REF_TEMPLATE = "                $ref: '#/components/schemas/" + COMPONENT_NAME_VAR + "'";
  private static final String YAML_RESPONSE_PATH_SCHEMA_REF_INCLUDE_ROOT_TEMPLATE = "                $ref: '#/components/schemas/" + COMPONENT_NAME_VAR + "IncludeRoot'";
  private static final String YAML_COMPONENT_SCHEMA_TEMPLATE = "    " + COMPONENT_NAME_VAR + ":";
  private static final String YAML_COMPONENT_SCHEMA_INCLUDE_ROOT_TEMPLATE = "    " + COMPONENT_NAME_VAR + "IncludeRoot:";

  private static final String JSON_COMPONENTS_ENTRIES = "\"components\" : {";
  private static final String JSON_PATH_SCHEMA_REF_TEMPLATE = "\"$ref\" : \"#/components/schemas/" + COMPONENT_NAME_VAR + "\"";
  private static final String JSON_PATH_SCHEMA_REF_INCLUDE_ROOT_TEMPLATE = "\"$ref\" : \"#/components/schemas/" + COMPONENT_NAME_VAR + "IncludeRoot\"";
  private static final String JSON_COMPONENT_SCHEMA_TEMPLATE = "\"" + COMPONENT_NAME_VAR + "\" : {";
  private static final String JSON_COMPONENT_SCHEMA_INCLUDE_ROOT_TEMPLATE = "\"" + COMPONENT_NAME_VAR + "IncludeRoot\" : {";


  @Test
  public void openapiYAMLAcceptHeaderOpenApiResource()
  {
    CustomSpecFilter.disableIncludeRootSpecFilter();

    WebTarget target = target("openapi");
    String openapiYaml = target.request(APPLICATION_YAML_TYPE).get(String.class);

    assertThat(openapiYaml).contains("openapi: 3.0.1");
    assertYamlIncludeRoot(openapiYaml, false);
  }


  @Test
  public void openapiYAMLAcceptHeaderOpenApiResourceIncludeRoot()
  {
    CustomSpecFilter.enableIncludeRootSpecFilter();

    WebTarget target = target("openapi");
    String openapiYaml = target.request(APPLICATION_YAML_TYPE).get(String.class);
    System.out.println(openapiYaml);

    assertThat(openapiYaml).contains("openapi: 3.0.1");
    assertYamlIncludeRoot(openapiYaml, true);
  }


  @Test
  public void openapiJSONAcceptHeaderOpenApiResource()
  {
    CustomSpecFilter.disableIncludeRootSpecFilter();

    WebTarget target = target("openapi");
    String openapiJson = target.request(MediaType.APPLICATION_JSON_TYPE).get(String.class);

    assertThat(openapiJson).contains("\"openapi\" : \"3.0.1\",");
    assertJsonIncludeRoot(openapiJson, false);
  }


  @Test
  public void openapiJSONAcceptHeaderOpenApiResourceIncludeRoot()
  {
    CustomSpecFilter.enableIncludeRootSpecFilter();

    WebTarget target = target("openapi");
    String openapiJson = target.request(MediaType.APPLICATION_JSON_TYPE).get(String.class);

    assertThat(openapiJson).contains("\"openapi\" : \"3.0.1\",");
    assertJsonIncludeRoot(openapiJson, true);
  }


  @Test
  public void openapiXMLAcceptHeaderOpenApiResource()
  {
    WebTarget target = target("openapi");
    Response response = target.request(MediaType.APPLICATION_XML_TYPE).get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_ACCEPTABLE);
  }


  @Test
  public void openapiXHTMLAcceptHeaderOpenApiResource()
  {
    WebTarget target = target("openapi");
    Response response = target.request(MediaType.APPLICATION_XHTML_XML_TYPE).get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_ACCEPTABLE);
  }


  @Test
  public void openapiYAMLOpenApiResource()
  {
    CustomSpecFilter.disableIncludeRootSpecFilter();

    WebTarget target = target("openapi.yaml");
    String openapiYaml = target.request().get(String.class);

    assertThat(openapiYaml).contains("openapi: 3.0.1");
    assertYamlIncludeRoot(openapiYaml, false);
  }


  @Test
  public void openapiYAMLOpenApiResourceIncludeRoot()
  {
    CustomSpecFilter.enableIncludeRootSpecFilter();

    WebTarget target = target("openapi.yaml");
    String openapiYaml = target.request().get(String.class);

    assertThat(openapiYaml).contains("openapi: 3.0.1");
    assertYamlIncludeRoot(openapiYaml, true);
  }


  @Test
  public void openapiJSONOpenApiResource()
  {
    CustomSpecFilter.disableIncludeRootSpecFilter();

    WebTarget target = target("openapi.json");
    String openapiJson = target.request().get(String.class);

    assertThat(openapiJson).contains("\"openapi\" : \"3.0.1\",");
    assertJsonIncludeRoot(openapiJson, false);
  }


  @Test
  public void openapiJSONOpenApiResourceIncludeRoot()
  {
    CustomSpecFilter.enableIncludeRootSpecFilter();

    WebTarget target = target("openapi.json");
    String openapiJson = target.request().get(String.class);

    assertThat(openapiJson).contains("\"openapi\" : \"3.0.1\",");
    assertJsonIncludeRoot(openapiJson, true);
  }


  @Test
  public void openapiXMLOpenApiResource()
  {
    WebTarget target = target("openapi.xml");
    Response response = target.request().get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_FOUND);
  }


  @Test
  public void openapiXHTMLOpenApiResource()
  {
    WebTarget target = target("openapi.xhtml");
    Response response = target.request().get();

    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.NOT_FOUND);
  }


  private void assertYamlIncludeRoot(String definition, boolean includeRoot)
  {
    String paths = StringUtils.substringBeforeLast(definition, YAML_COMPONENTS_ENTRIES);
    String components = StringUtils.substringAfterLast(definition, YAML_COMPONENTS_ENTRIES);

    for (String componentName : COMPONENT_NAMES)
    {
      boolean isRequestComponent = StringUtils.equals(componentName, REQUEST_COMPONENT_NAME);
      String refTemplate = isRequestComponent ? YAML_REQUEST_PATH_SCHEMA_REF_TEMPLATE : YAML_RESPONSE_PATH_SCHEMA_REF_TEMPLATE;
      String componentRef = StringUtils.replace(refTemplate, COMPONENT_NAME_VAR, componentName);
      String componentSchema = StringUtils.replace(YAML_COMPONENT_SCHEMA_TEMPLATE, COMPONENT_NAME_VAR, componentName);

      assertThat(paths).contains(componentRef);
      assertThat(components).contains(componentSchema);

      String refIncludeRootTemplate = isRequestComponent ? YAML_REQUEST_PATH_SCHEMA_REF_INCLUDE_ROOT_TEMPLATE : YAML_RESPONSE_PATH_SCHEMA_REF_INCLUDE_ROOT_TEMPLATE;
      String componentIncludeRootRef = StringUtils.replace(refIncludeRootTemplate, COMPONENT_NAME_VAR, componentName);
      String componentIncludeRootSchema = StringUtils.replace(YAML_COMPONENT_SCHEMA_INCLUDE_ROOT_TEMPLATE, COMPONENT_NAME_VAR, componentName);

      if (includeRoot)
      {
        assertThat(paths).contains(componentIncludeRootRef);
        assertThat(components).contains(componentIncludeRootSchema);
      }
      else
      {
        assertThat(paths).doesNotContain(componentIncludeRootRef);
        assertThat(components).doesNotContain(componentIncludeRootSchema);
      }
    }
  }


  private void assertJsonIncludeRoot(String definition, boolean includeRoot)
  {
    String paths = StringUtils.substringBeforeLast(definition, JSON_COMPONENTS_ENTRIES);
    String components = StringUtils.substringAfterLast(definition, JSON_COMPONENTS_ENTRIES);

    for (String componentName : COMPONENT_NAMES)
    {
      String componentRef = StringUtils.replace(JSON_PATH_SCHEMA_REF_TEMPLATE, COMPONENT_NAME_VAR, componentName);
      String componentSchema = StringUtils.replace(JSON_COMPONENT_SCHEMA_TEMPLATE, COMPONENT_NAME_VAR, componentName);

      assertThat(paths).contains(componentRef);
      assertThat(components).contains(componentSchema);

      String componentIncludeRootRef = StringUtils.replace(JSON_PATH_SCHEMA_REF_INCLUDE_ROOT_TEMPLATE, COMPONENT_NAME_VAR, componentName);
      String componentIncludeRootSchema = StringUtils.replace(JSON_COMPONENT_SCHEMA_INCLUDE_ROOT_TEMPLATE, COMPONENT_NAME_VAR, componentName);

      if (includeRoot)
      {
        assertThat(paths).contains(componentIncludeRootRef);
        assertThat(components).contains(componentIncludeRootSchema);
      }
      else
      {
        assertThat(paths).doesNotContain(componentIncludeRootRef);
        assertThat(components).doesNotContain(componentIncludeRootSchema);
      }
    }
  }


  @Test
  public void sampleJSON()
  {
    final WebTarget target = target("/api/v1/sample");
    final SampleInfo sampleInfo = target.request(MediaType.APPLICATION_JSON_TYPE).get(SampleInfo.class);

    assertThat(sampleInfo.getName()).isEqualTo("Sample Swagger Annotation and The OpenAPI Specification.");
    assertThat(sampleInfo.getCompany()).isEqualTo("Free Dumb Bytes aka freedumbytes");
    assertThat(sampleInfo.getVersion()).isEqualTo("0.1.0");
    assertThat(sampleInfo.getBuild()).isEqualTo("2019-02-28 17:26:00 CET").containsPattern(".CET|.CEST");
    assertThat(sampleInfo.getTime()).isNotNull();
  }


  @Test
  public void sampleAnnotatedJSON()
  {
    final WebTarget target = target("/api/v1/sampleAnnotated");
    final SampleInfo sampleInfo = target.request(MediaType.APPLICATION_JSON_TYPE).get(SampleInfo.class);

    assertThat(sampleInfo.getName()).isEqualTo("Sample Swagger Annotation and The OpenAPI Specification.");
    assertThat(sampleInfo.getCompany()).isEqualTo("Free Dumb Bytes aka freedumbytes");
    assertThat(sampleInfo.getVersion()).isEqualTo("0.1.0");
    assertThat(sampleInfo.getBuild()).isEqualTo("2019-02-28 17:26:00 CET").containsPattern(".CET|.CEST");
    assertThat(sampleInfo.getTime()).isNotNull();
  }


  @Test
  public void sampleAnnotatedAndNamedJSON()
  {
    final WebTarget target = target("/api/v1/sampleAnnotatedAndNamed");
    final SampleNamedInfo sampleNamedInfo = target.request(MediaType.APPLICATION_JSON_TYPE).get(SampleNamedInfo.class);

    assertThat(sampleNamedInfo.getName()).isEqualTo("Sample Swagger Annotation and The OpenAPI Specification.");
    assertThat(sampleNamedInfo.getCompany()).isEqualTo("Free Dumb Bytes aka freedumbytes");
    assertThat(sampleNamedInfo.getVersion()).isEqualTo("0.1.0");
    assertThat(sampleNamedInfo.getBuild()).isEqualTo("2019-02-28 17:26:00 CET").containsPattern(".CET|.CEST");
    assertThat(sampleNamedInfo.getTime()).isNotNull();
  }


  @Test
  public void updateSampleAppJSON()
  {
    final SampleApplication application = SampleApplication.of("bball", "Sample Application.");
    final WebTarget target = target("/api/v1/updateSampleApp");
    final Response reponse = target.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(application, MediaType.APPLICATION_JSON_TYPE));
    final SampleInfo sampleInfo = reponse.readEntity(SampleInfo.class);

    assertThat(sampleInfo.getName()).isEqualTo("Sample Application.");
    assertThat(sampleInfo.getCompany()).isEqualTo("Free Dumb Bytes aka freedumbytes");
    assertThat(sampleInfo.getVersion()).isEqualTo("0.1.0");
    assertThat(sampleInfo.getBuild()).isEqualTo("2019-02-28 17:26:00 CET").containsPattern(".CET|.CEST");
    assertThat(sampleInfo.getTime()).isNotNull();
  }


  @Test
  public void updateSampleAppAnnotatedJSON()
  {
    final SampleApplication application = SampleApplication.of("bball", "Sample Application.");
    final WebTarget target = target("/api/v1/updateSampleAppAnnotated");
    final Response reponse = target.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(application, MediaType.APPLICATION_JSON_TYPE));
    final SampleInfo sampleInfo = reponse.readEntity(SampleInfo.class);

    assertThat(sampleInfo.getName()).isEqualTo("Sample Application.");
    assertThat(sampleInfo.getCompany()).isEqualTo("Free Dumb Bytes aka freedumbytes");
    assertThat(sampleInfo.getVersion()).isEqualTo("0.1.0");
    assertThat(sampleInfo.getBuild()).isEqualTo("2019-02-28 17:26:00 CET").containsPattern(".CET|.CEST");
    assertThat(sampleInfo.getTime()).isNotNull();
  }


  @Override
  protected void configureClient(ClientConfig clientConfig)
  {
    ResourceConfig serverConfig = getResourceConfigServer();
    new JerseyConfigBuilder().configureClientBasedOnServer(clientConfig, serverConfig);
  }


  @Override
  protected Application configure()
  {
    return getResourceConfigServer();
  }


  private ResourceConfig getResourceConfigServer()
  {
    JerseyConfigBuilder configBuilder = new JerseyConfigBuilder();
    configBuilder.enableLogTrafficDumpEntityLoggingFeatureAlternative();
    configBuilder.enableMoxyXmlFeature();
    configBuilder.enableMoxyJsonIncludeRoot();

    ResourceConfig serverConfig = new ResourceConfig(SampleResource.class, CurrentTimeResource.class, InterfaceResource.class, EchoResource.class, //
        WadlResource.class, OpenApiResource.class, AcceptHeaderOpenApiResource.class, CustomParamConverterProvider.class);

    configBuilder.configureServer(serverConfig);

    return serverConfig;
  }
}
