package nl.demon.shadowland.freedumbytes.rest.param.assignable.test;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


@NoArgsConstructor
@Getter
@Setter
@Accessors(fluent = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Sibling extends Parent implements SiblingInterface
{
  private String message;


  public Sibling(String value)
  {
    value(value);
  }


  public boolean noIWasNot()
  {
    return !SiblingConverter.KILROY_SIGNATURE.equals(message);
  }
}
