package nl.demon.shadowland.freedumbytes.rest.param.test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.inject.Singleton;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


/**
 * There seems to be an Echo in here.
 *
 * <p>
 * Issue - Java 8 Date/Time API classes cause:
 * </p>
 *
 * <pre>
 * {@code
 *   org.glassfish.jersey.server.model.ModelValidationException: Validation of the application resource model has failed during application initialization.
 *     [[FATAL] No injection source found for a parameter of type public nl.demon.shadowland.freedumbytes.rest.param.test.Echo
 *     nl.demon.shadowland.freedumbytes.rest.param.test.EchoResource.echo(java.time.LocalTime) at index 0.;}
 * </pre>
 *
 * <p>
 * Solution - Custom {@code @Provider} implements {@code ParamConverterProvider} and Java 8 Local Date/Time API converters.
 * </p>
 */
@Path("/echo")
@Singleton
public class EchoResource
{
  @GET
  @Path("/string")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Echo echo(@DefaultValue("Undefined") @QueryParam("echo") String text)
  {
    Echo echo = new Echo();
    echo.setText(text);

    return echo;
  }


  @GET
  @Path("/localDate")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Echo echo(@QueryParam("echo") LocalDate localDate)
  {
    Echo echo = new Echo();
    echo.setLocalDate(localDate);

    return echo;
  }


  @GET
  @Path("/localDateTime")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Echo echo(@QueryParam("echo") LocalDateTime localDateTime)
  {
    Echo echo = new Echo();
    echo.setLocalDateTime(localDateTime);

    return echo;
  }


  @GET
  @Path("/localTime")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Echo echo(@QueryParam("echo") LocalTime localTime)
  {
    Echo echo = new Echo();
    echo.setLocalTime(localTime);

    return echo;
  }
}
