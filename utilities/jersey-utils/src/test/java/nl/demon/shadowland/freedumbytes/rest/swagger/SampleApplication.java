package nl.demon.shadowland.freedumbytes.rest.swagger;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@XmlRootElement(name = "app")
@XmlType(propOrder = { "id", "name" })
@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
@Getter
@Setter
@ToString
@Tag(name = "demo")
@Schema(description = "Application information.")
public class SampleApplication
{

  @NonNull
  @Schema(description = "Application id.", example = "bball")
  private String id;

  @NonNull
  @Schema(description = "Application name.", example = "Sample Application")
  private String name;
}
