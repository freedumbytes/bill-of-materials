package nl.demon.shadowland.freedumbytes.rest.config;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.ext.ContextResolver;

import org.eclipse.persistence.jaxb.BeanValidationMode;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.oxm.XMLConstants;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.moxy.xml.MoxyXmlFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.rest.swagger.filter.CustomSpecFilter;
import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;


public abstract class JerseyConfigBuilderTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(JerseyConfigBuilder.class);

  private JerseyConfigBuilder configBuilder;
  private ResourceConfig serverConfig;
  private ClientConfig clientConfig;


  @Before()
  public void createConfig()
  {
    configBuilder = new JerseyConfigBuilder();
    serverConfig = new ResourceConfig();
    clientConfig = new ClientConfig();
  }


  @Test
  public void enableLogTrafficDumpEntityLoggingFeatureWorkaround()
  {
    initializeConfig();

    configBuilder.enableLogTrafficDumpEntityLoggingFeatureWorkaround();

    reinitializeConfig();

    assertThat(serverConfig.getInstances()).hasSize(1);
    assertThat(clientConfig.getInstances()).hasSize(1);

    assertThat(serverConfig.getInstances().toArray()[0]).isInstanceOf(LoggingFeature.class);
    assertThat(clientConfig.getInstances().toArray()[0]).isInstanceOf(LoggingFeature.class);
  }


  @Test
  public void enableLogTrafficDumpEntityLoggingFeatureAlternative()
  {
    initializeConfig();

    configBuilder.enableLogTrafficDumpEntityLoggingFeatureAlternative();

    reinitializeConfig();

    assertThat(serverConfig.getInstances()).hasSize(1);
    assertThat(clientConfig.getInstances()).hasSize(1);

    assertThat(serverConfig.getInstances().toArray()[0]).isInstanceOf(LoggingFeature.class);
    assertThat(clientConfig.getInstances().toArray()[0]).isInstanceOf(LoggingFeature.class);
  }


  @Test
  public void enableMoxyXmlFeature()
  {
    initializeConfig();

    configBuilder.enableMoxyXmlFeature();

    reinitializeConfig();

    assertMoxyConfig(serverConfig, clientConfig, false, false, BeanValidationMode.NONE);
  }


  @Test
  public void enableMoxyJsonIncludeRoot()
  {
    initializeConfig();

    configBuilder.enableMoxyJsonIncludeRoot();

    reinitializeConfig();

    assertMoxyConfig(serverConfig, clientConfig, true, false, BeanValidationMode.NONE);
  }


  @Test
  public void enableMoxyJsonFormattedOutput()
  {
    initializeConfig();

    configBuilder.enableMoxyJsonFormattedOutput();

    reinitializeConfig();

    assertMoxyConfig(serverConfig, clientConfig, false, true, BeanValidationMode.NONE);
  }


  @Test
  public void beanValidationMode()
  {
    initializeConfig();

    configBuilder.beanValidationMode(BeanValidationMode.AUTO);

    reinitializeConfig();

    assertMoxyConfig(serverConfig, clientConfig, false, false, BeanValidationMode.AUTO);
  }


  @Test
  public void enableBeanValidationSendErrorInResponse()
  {
    initializeConfig();

    configBuilder.enableBeanValidationSendErrorInResponse();

    assertThat(serverConfig.getProperty(ServerProperties.BV_SEND_ERROR_IN_RESPONSE)).isEqualTo(Boolean.FALSE);
    assertThat(clientConfig.getProperty(ServerProperties.BV_SEND_ERROR_IN_RESPONSE)).isNull();

    reinitializeConfig();

    assertThat(serverConfig.getInstances()).isEmpty();
    assertThat(clientConfig.getInstances()).isEmpty();

    assertThat(serverConfig.getProperty(ServerProperties.BV_SEND_ERROR_IN_RESPONSE)).isEqualTo(Boolean.TRUE);
    assertThat(clientConfig.getProperty(ServerProperties.BV_SEND_ERROR_IN_RESPONSE)).isNull();
  }


  @Test
  public void disableWadlFeature()
  {
    initializeConfig();

    assertThat(serverConfig.getProperty(ServerProperties.WADL_FEATURE_DISABLE)).isEqualTo(Boolean.FALSE);
    assertThat(clientConfig.getProperty(ServerProperties.WADL_FEATURE_DISABLE)).isNull();

    configBuilder.disableWadlFeature();

    reinitializeConfig();

    assertThat(serverConfig.getInstances()).isEmpty();
    assertThat(clientConfig.getInstances()).isEmpty();

    assertThat(serverConfig.getProperty(ServerProperties.WADL_FEATURE_DISABLE)).isEqualTo(Boolean.TRUE);
    assertThat(clientConfig.getProperty(ServerProperties.WADL_FEATURE_DISABLE)).isNull();
  }


  @Test
  public void configureClientBasedOnServer()
  {
    configBuilder.configureServer(serverConfig);
    configBuilder.configureClientBasedOnServer(clientConfig, serverConfig);

    assertThat(serverConfig.getInstances()).isEmpty();
    assertThat(clientConfig.getInstances()).isEmpty();

    configBuilder.enableLogTrafficDumpEntityLoggingFeatureWorkaround();
    configBuilder.enableLogTrafficDumpEntityLoggingFeatureAlternative();
    configBuilder.enableMoxyXmlFeature();

    configBuilder.configureServer(serverConfig);
    configBuilder.configureClientBasedOnServer(clientConfig, serverConfig);

    assertThat(serverConfig.getInstances()).hasSize(3);
    assertThat(clientConfig.getInstances()).hasSize(3);

    Set<Object> clientRegisteredInstances = clientConfig.getInstances();
    serverConfig.getInstances().forEach(instance -> assertThat(clientRegisteredInstances).contains(instance));
  }


  private void initializeConfig()
  {
    configBuilder.configureServer(serverConfig);
    configBuilder.configureClient(clientConfig);

    assertThat(serverConfig.getInstances()).isEmpty();
    assertThat(clientConfig.getInstances()).isEmpty();
  }


  private void reinitializeConfig()
  {
    configBuilder.configureServer(serverConfig);
    configBuilder.configureClient(clientConfig);
  }


  private void assertMoxyConfig(ResourceConfig serverConfig, ClientConfig clientConfig, boolean includeRoot, boolean formattedOutput, BeanValidationMode beanValidationMode)
  {
    assertThat(serverConfig.getInstances()).hasSize(2);
    assertThat(clientConfig.getInstances()).hasSize(2);

    assertThat(filterOnInstanceOfClass(serverConfig, MoxyXmlFeature.class)).hasSize(1);
    assertThat(filterOnInstanceOfClass(clientConfig, MoxyXmlFeature.class)).hasSize(1);

    MoxyJsonConfig serverMoxyJsonConfig = filterOnlyInstanceOfClassWrappedInContextResolver(serverConfig, MoxyJsonConfig.class);
    MoxyJsonConfig clientMoxyJsonConfig = filterOnlyInstanceOfClassWrappedInContextResolver(clientConfig, MoxyJsonConfig.class);

    assertThat(serverMoxyJsonConfig.getNamespaceSeparator()).isEqualByComparingTo(XMLConstants.COLON);
    assertThat(clientMoxyJsonConfig.getNamespaceSeparator()).isEqualByComparingTo(XMLConstants.COLON);

    assertThat(serverMoxyJsonConfig.isIncludeRoot()).isEqualTo(includeRoot);
    assertThat(clientMoxyJsonConfig.isIncludeRoot()).isEqualTo(includeRoot);

    if (includeRoot)
    {
      assertThat(CustomSpecFilter.isEnabledIncludeRootSpecFilter()).isTrue();
    }
    else
    {
      assertThat(CustomSpecFilter.isEnabledIncludeRootSpecFilter()).isFalse();
    }

    assertThat(serverMoxyJsonConfig.isFormattedOutput()).isEqualTo(formattedOutput);
    assertThat(clientMoxyJsonConfig.isFormattedOutput()).isEqualTo(formattedOutput);

    assertThat(serverMoxyJsonConfig.getMarshallerProperties().get(MarshallerProperties.BEAN_VALIDATION_MODE)).isEqualTo(beanValidationMode);
    assertThat(serverMoxyJsonConfig.getUnmarshallerProperties().get(MarshallerProperties.BEAN_VALIDATION_MODE)).isEqualTo(beanValidationMode);
    assertThat(clientMoxyJsonConfig.getMarshallerProperties().get(MarshallerProperties.BEAN_VALIDATION_MODE)).isEqualTo(beanValidationMode);
    assertThat(clientMoxyJsonConfig.getUnmarshallerProperties().get(MarshallerProperties.BEAN_VALIDATION_MODE)).isEqualTo(beanValidationMode);
  }


  private <T> T filterOnlyInstanceOfClassWrappedInContextResolver(ResourceConfig serverConfig, Class<T> theClass)
  {
    return filterOnlyInstanceOfClassWrappedInContextResolver(serverConfig.getInstances(), theClass);
  }


  private <T> T filterOnlyInstanceOfClassWrappedInContextResolver(ClientConfig clientConfig, Class<T> theClass)
  {
    return filterOnlyInstanceOfClassWrappedInContextResolver(clientConfig.getInstances(), theClass);
  }


  private <T> T filterOnlyInstanceOfClassWrappedInContextResolver(Set<Object> set, Class<T> theClass)
  {
    Stream<ContextResolver<Object>> contextResolverStream = set.stream().filter(ContextResolver.class::isInstance).map(ContextResolver.class::cast);
    contextResolverStream = contextResolverStream.filter(cr -> cr.getContext(theClass.getClass()).getClass().isAssignableFrom(theClass));
    Stream<T> theClassStream = contextResolverStream.map(cr -> cr.getContext(theClass)).map(theClass::cast);
    List<T> list = theClassStream.collect(Collectors.toList());

    assertThat(list).hasSize(1);

    return list.get(0);
  }


  private Set<Object> filterOnInstanceOfClass(ResourceConfig serverConfig, Class<?> theClass)
  {
    return filterOnInstanceOfClass(serverConfig.getInstances(), theClass);
  }


  private Set<Object> filterOnInstanceOfClass(ClientConfig clientConfig, Class<?> theClass)
  {
    return filterOnInstanceOfClass(clientConfig.getInstances(), theClass);
  }


  private Set<Object> filterOnInstanceOfClass(Set<Object> set, Class<?> theClass)
  {
    return set.stream().filter(theClass::isInstance).collect(Collectors.toSet());
  }
}
