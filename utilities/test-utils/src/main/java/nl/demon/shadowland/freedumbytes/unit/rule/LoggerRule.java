package nl.demon.shadowland.freedumbytes.unit.rule;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.rules.ExternalResource;
import org.mockito.ArgumentCaptor;
import org.mockito.verification.VerificationMode;


/**
 * Assertions for <a href="https://logging.apache.org/log4j/" target="_blank" rel="noopener noreferrer">Log4j</a> logging behaviour in unit tests.
 *
 * @see <a href="https://slackhacker.com/2009/12/08/testing-logging-behaviour-in-four-code-lines-flat/" target="_blank" rel="noopener noreferrer">Testing logging behaviour in four code lines flat</a>
 * @see <a href="http://alexandregiannini.blogspot.com/2015/04/testing-logging-behavior-is.html" target="_blank" rel="noopener noreferrer">Easy verifying of logging events in jUnit tests with Mockito</a>
 * @see <a href="https://github.com/junit-team/junit4/wiki/rules" target="_blank" rel="noopener noreferrer">Rules allow very flexible addition or redefinition of the behavior of each test method in a test class</a>
 */
public class LoggerRule extends ExternalResource
{
  private Appender appender;
  private String name;
  private Level originalLevel;


  public LoggerRule(Class<?> clazz)
  {
    this(clazz.getName());
  }


  public LoggerRule(String name)
  {
    this.name = name;
  }


  public void verifyLoggingEventNever()
  {
    verifyLoggingEvent(never());
  }


  public void verifyLoggingEventNever(Level level)
  {
    List<LoggingEvent> events = verifyLoggingEventsWithFilter(level);

    assertThat(events).hasSize(0);
  }


  public LoggingEvent verifyLoggingEvent(Level level)
  {
    List<LoggingEvent> events = verifyLoggingEventsWithFilter(level);

    assertThat(events).hasSize(1);

    return events.get(0);
  }


  public List<LoggingEvent> verifyLoggingEvents(Level level)
  {
    List<LoggingEvent> events = verifyLoggingEventsWithFilter(level);

    assertThat(events.size()).isGreaterThanOrEqualTo(1);

    return events;
  }


  public List<LoggingEvent> verifyLoggingEvents(Level level, int wantedNumberOfInvocations)
  {
    List<LoggingEvent> events = verifyLoggingEventsWithFilter(level);

    assertThat(events.size()).isEqualTo(wantedNumberOfInvocations);

    return events;
  }


  private List<LoggingEvent> verifyLoggingEventsWithFilter(Level level)
  {
    List<LoggingEvent> events = verifyLoggingEvents();
    events = filter(events, level);

    return events;
  }


  private static List<LoggingEvent> filter(List<LoggingEvent> events, Level level)
  {
    return events.stream().filter(evt -> evt.getLevel().equals(level)).collect(Collectors.toList());
  }


  public LoggingEvent verifyLoggingEvent()
  {
    return verifyLoggingEvent(times(1)).getValue();
  }


  public List<LoggingEvent> verifyLoggingEvents()
  {
    return verifyLoggingEvent(atLeastOnce()).getAllValues();
  }


  public List<LoggingEvent> verifyLoggingEvents(VerificationMode mode)
  {
    return verifyLoggingEvent(mode).getAllValues();
  }


  private ArgumentCaptor<LoggingEvent> verifyLoggingEvent(VerificationMode mode)
  {
    ArgumentCaptor<LoggingEvent> captor = ArgumentCaptor.forClass(LoggingEvent.class);
    verify(getAppender(), mode).doAppend(captor.capture());

    return captor;
  }


  public Appender getAppender()
  {
    return appender;
  }


  @Override
  protected void before() throws Throwable
  {
    appender = mock(Appender.class);

    getLogger().addAppender(appender);

    originalLevel = getLogger().getLevel();
    getLogger().setLevel(Level.ALL);
  }


  @Override
  protected void after()
  {
    getLogger().removeAppender(appender);
    getLogger().setLevel(originalLevel);
  }


  public void withLevel(Level level)
  {
    getLogger().setLevel(level);
  }


  private Logger getLogger()
  {
    return LogManager.getLogger(name);
  }
}
