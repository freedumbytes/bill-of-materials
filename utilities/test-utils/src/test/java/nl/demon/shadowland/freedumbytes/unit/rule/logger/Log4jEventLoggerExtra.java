package nl.demon.shadowland.freedumbytes.unit.rule.logger;


import lombok.extern.log4j.Log4j;


@Log4j
public class Log4jEventLoggerExtra
{
  public void logExtra()
  {
    log.fatal("fatal extra message");
  }
}
