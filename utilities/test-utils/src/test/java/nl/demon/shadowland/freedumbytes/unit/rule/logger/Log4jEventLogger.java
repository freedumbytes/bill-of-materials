package nl.demon.shadowland.freedumbytes.unit.rule.logger;


import lombok.extern.log4j.Log4j;


/**
 * Apache Log4j is a Java-based logging utility.
 */
@Log4j
public class Log4jEventLogger
{
  public void logNothing()
  {
  }


  public void logError()
  {
    log.error("error message");
  }


  public void logAll()
  {
    log.fatal("fatal message");
    log.error("error message");
    log.warn("warn message");
    log.info("info message");
    log.debug("debug message");
    log.trace("trace message");
  }


  public void logExtraClass()
  {
    logAll();

    new Log4jEventLoggerExtra().logExtra();
  }
}
