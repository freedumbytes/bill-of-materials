package nl.demon.shadowland.freedumbytes.unit.rule.logger;


import lombok.extern.apachecommons.CommonsLog;


/**
 * Apache Commons Logging (previously known as Jakarta Commons Logging or JCL).
 */
@CommonsLog
public class CommonsLoggingEventLogger
{
  public void logAll()
  {
    log.fatal("fatal message");
    log.error("error message");
    log.warn("warn message");
    log.info("info message");
    log.debug("debug message");
    log.trace("trace message");
  }
}
