package nl.demon.shadowland.freedumbytes.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.logging.Logger;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.bridge.SLF4JBridgeHandler;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;


public class JavaUtilLoggingLoggerRuleTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(JavaUtilLoggingEventLogger.class);

  private JavaUtilLoggingEventLogger eventLogger = new JavaUtilLoggingEventLogger();


  @Test
  public void logAll()
  {
    activateSlf4jBridge();

    eventLogger.logAll();

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(7));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo("severe message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(1).getMessage()).isEqualTo("warning message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(2).getMessage()).isEqualTo("info message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage()).isEqualTo("config message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage()).isEqualTo("fine message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(5).getMessage()).isEqualTo("finer message");

    assertThat(events.get(6).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(6).getMessage()).isEqualTo("finest message");
  }


  private void activateSlf4jBridge()
  {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
    Logger.getLogger(JavaUtilLoggingEventLogger.class.getName()).setLevel(java.util.logging.Level.FINEST);
  }
}
