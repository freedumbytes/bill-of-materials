package nl.demon.shadowland.freedumbytes.unit.rule.logger;


import lombok.extern.slf4j.Slf4j;


/**
 * The Simple Logging Facade for Java (SLF4J) serves as a simple facade or abstraction for various logging frameworks.
 */
@Slf4j
public class Slf4jEventLogger
{
  public void logAll()
  {
    log.error("error message");
    log.warn("warn message");
    log.info("info message");
    log.debug("debug message");
    log.trace("trace message");
  }
}
