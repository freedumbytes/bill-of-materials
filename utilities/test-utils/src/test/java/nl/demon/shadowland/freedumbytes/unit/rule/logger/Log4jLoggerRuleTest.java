package nl.demon.shadowland.freedumbytes.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;


public class Log4jLoggerRuleTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Log4jEventLogger.class);

  private Log4jEventLogger eventLogger = new Log4jEventLogger();


  @Test
  public void logNothing()
  {
    eventLogger.logNothing();

    loggerRule.verifyLoggingEventNever();
  }


  @Test
  public void logError()
  {
    eventLogger.logError();

    LoggingEvent event = loggerRule.verifyLoggingEvent();

    assertThat(event.getLevel()).isEqualTo(Level.ERROR);
    assertThat(event.getMessage()).isEqualTo("error message");
  }


  @Test
  public void logErrorNoFatal()
  {
    eventLogger.logError();

    loggerRule.verifyLoggingEventNever(Level.FATAL);
  }


  @Test
  public void logErrorFilterFatal()
  {
    eventLogger.logError();

    loggerRule.verifyLoggingEvents(Level.FATAL, 0);
  }


  @Test
  public void logErrorFilterError()
  {
    eventLogger.logError();

    LoggingEvent event = loggerRule.verifyLoggingEvent(Level.ERROR);

    assertThat(event.getLevel()).isEqualTo(Level.ERROR);
    assertThat(event.getMessage()).isEqualTo("error message");
  }


  @Test
  public void logAllFilterErrors()
  {
    eventLogger.logError();

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(Level.ERROR);

    assertThat(events).hasSize(1);
    assertThat(events.get(0).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo("error message");
  }


  @Test
  public void logAll()
  {
    eventLogger.logAll();

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(6));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage()).isEqualTo("info message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage()).isEqualTo("debug message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(5).getMessage()).isEqualTo("trace message");
  }


  @Test
  public void logExtraClass()
  {
    eventLogger.logExtraClass();

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(6));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage()).isEqualTo("info message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(4).getMessage()).isEqualTo("debug message");

    assertThat(events.get(5).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(5).getMessage()).isEqualTo("trace message");
  }


  @Test
  public void dontLogAllWithLevelInfo()
  {
    loggerRule.withLevel(Level.INFO);

    eventLogger.logAll();

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(4));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.FATAL);
    assertThat(events.get(0).getMessage()).isEqualTo("fatal message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(1).getMessage()).isEqualTo("error message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(2).getMessage()).isEqualTo("warn message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(3).getMessage()).isEqualTo("info message");
  }
}
