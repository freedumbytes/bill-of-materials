package nl.demon.shadowland.freedumbytes.unit.rule.logger;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;


public class Slf4jLoggerRuleTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Slf4jEventLogger.class);

  private Slf4jEventLogger eventLogger = new Slf4jEventLogger();


  @Test
  public void logAll()
  {
    eventLogger.logAll();

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(5));

    assertThat(events.get(0).getLevel()).isEqualTo(Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo("error message");

    assertThat(events.get(1).getLevel()).isEqualTo(Level.WARN);
    assertThat(events.get(1).getMessage()).isEqualTo("warn message");

    assertThat(events.get(2).getLevel()).isEqualTo(Level.INFO);
    assertThat(events.get(2).getMessage()).isEqualTo("info message");

    assertThat(events.get(3).getLevel()).isEqualTo(Level.DEBUG);
    assertThat(events.get(3).getMessage()).isEqualTo("debug message");

    assertThat(events.get(4).getLevel()).isEqualTo(Level.TRACE);
    assertThat(events.get(4).getMessage()).isEqualTo("trace message");
  }
}
