package nl.demon.shadowland.freedumbytes.rest.test.spring;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;


/**
 * Sample Spring wiring.
 */
@Configuration
@ComponentScan(basePackages = { "nl.demon.shadowland.freedumbytes.rest.test.spring" })
@Slf4j
public class SpringConfigEcho
{
  public SpringConfigEcho()
  {
    log.info("Run Spring Echo REST wiring.");
  }
}
