package nl.demon.shadowland.freedumbytes.rest.test.spring;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalTime;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import nl.demon.shadowland.freedumbytes.rest.config.JerseyConfigBuilder;
import nl.demon.shadowland.freedumbytes.rest.test.JerseySpringIT;


/**
 * Demonstrate {@link JerseySpringIT} implementation with @Autowired JerseyConfigEchoServer.
 */
@ContextConfiguration(classes = { SpringConfigEcho.class })
public class EchoResourceAutowiredIT extends JerseySpringIT
{
  @Autowired
  private JerseyConfigEchoServer jerseyConfigServer;


  @Test
  public void echoLocalTimeJson()
  {
    echoLocalTime(MediaType.APPLICATION_JSON_TYPE);
  }


  private void echoLocalTime(MediaType mediaType)
  {
    LocalTime now = LocalTime.now();
    WebTarget target = target("echo/localTime").queryParam("echo", now);
    Echo echo = target.request(mediaType).get(Echo.class);

    assertThat(echo.getLocalTime()).isEqualTo(now);
  }


  @Override
  protected ResourceConfig getResourceConfigServer()
  {
    JerseyConfigBuilder.globallyEnableSlf4jBridge();

    return jerseyConfigServer;
  }
}
