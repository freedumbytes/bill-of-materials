package nl.demon.shadowland.freedumbytes.rest.test.jersey;


import java.time.LocalTime;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


@Path("/echo")
@Singleton
public class EchoResource
{
  @GET
  @Path("/localTime")
  @Produces({ MediaType.APPLICATION_XHTML_XML, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public Echo echo(@QueryParam("echo") LocalTime localTime)
  {
    Echo echo = new Echo();
    echo.setLocalTime(localTime);

    return echo;
  }
}
