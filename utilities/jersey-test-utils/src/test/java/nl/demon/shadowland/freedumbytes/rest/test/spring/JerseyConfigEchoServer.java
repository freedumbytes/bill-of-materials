package nl.demon.shadowland.freedumbytes.rest.test.spring;


import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import nl.demon.shadowland.freedumbytes.rest.config.JerseyConfigBuilder;

import lombok.extern.slf4j.Slf4j;


/**
 * Sample Jersey Server configuration.
 */
@Configuration
@Slf4j
public class JerseyConfigEchoServer extends ResourceConfig
{
  public JerseyConfigEchoServer()
  {
    log.info("Run Jersey Echo REST wiring.");

    packages("nl.demon.shadowland.freedumbytes.rest.test.spring", "nl.demon.shadowland.freedumbytes.rest.param");

    JerseyConfigBuilder builder = new JerseyConfigBuilder();

    builder.enableMoxyXmlFeature();
    builder.enableMoxyJsonIncludeRoot();
    builder.enableMoxyJsonFormattedOutput();
    builder.enableLogTrafficDumpEntityLoggingFeatureAlternative();
    builder.enableBeanValidationSendErrorInResponse();

    builder.configureServer(this);
  }
}
