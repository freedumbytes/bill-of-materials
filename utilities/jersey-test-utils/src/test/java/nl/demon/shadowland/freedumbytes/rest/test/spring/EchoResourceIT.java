package nl.demon.shadowland.freedumbytes.rest.test.spring;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalTime;

import javax.annotation.Resource;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import nl.demon.shadowland.freedumbytes.rest.config.JerseyConfigBuilder;
import nl.demon.shadowland.freedumbytes.rest.test.JerseySpringIT;


/**
 * Demonstrate {@link JerseySpringIT} implementation with @Resource JerseyConfigEchoServer.
 */
@ContextConfiguration(classes = { SpringConfigEcho.class })
public class EchoResourceIT extends JerseySpringIT
{
  @Resource
  private JerseyConfigEchoServer jerseyConfigServer;


  @Test
  public void echoLocalTimeJson()
  {
    echoLocalTime(MediaType.APPLICATION_JSON_TYPE);
  }


  @Test
  public void echoLocalTimeXml()
  {
    echoLocalTime(MediaType.APPLICATION_XML_TYPE);
  }


  private void echoLocalTime(MediaType mediaType)
  {
    LocalTime now = LocalTime.now();
    WebTarget target = target("echo/localTime").queryParam("echo", now);
    Echo echo = target.request(mediaType).get(Echo.class);

    assertThat(echo.getLocalTime()).isEqualTo(now);
  }


  @Test
  public void jaxbContext() throws JAXBException
  {
    assertThat(JAXBContext.newInstance(EchoResource.class).getClass().getName()).isEqualTo("com.sun.xml.internal.bind.v2.runtime.JAXBContextImpl");
    assertThat(JAXBContext.newInstance(Echo.class).getClass().getName()).isEqualTo("com.sun.xml.internal.bind.v2.runtime.JAXBContextImpl");
  }


  @Override
  protected ResourceConfig getResourceConfigServer()
  {
    JerseyConfigBuilder.globallyEnableSlf4jBridge();

    return jerseyConfigServer;
  }
}
