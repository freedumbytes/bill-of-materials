package nl.demon.shadowland.freedumbytes.rest.test.jersey;


import java.time.LocalTime;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@XmlRootElement
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Echo
{
  private LocalTime localTime;
}
