package nl.demon.shadowland.freedumbytes.rest.test;


import javax.ws.rs.core.Application;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

import nl.demon.shadowland.freedumbytes.rest.config.JerseyConfigBuilder;


/**
 * {@code JerseyIT} extends {@code JerseyTest} and configures the client {@code ClientConfig} based on the server {@code ResourceConfig}.
 */
public abstract class JerseyIT extends JerseyTest
{
  @Override
  protected Application configure()
  {
    return getResourceConfigServer();
  }


  @Override
  protected void configureClient(ClientConfig clientConfig)
  {
    ResourceConfig serverConfig = getResourceConfigServer();
    new JerseyConfigBuilder().configureClientBasedOnServer(clientConfig, serverConfig);
  }


  protected abstract ResourceConfig getResourceConfigServer();
}
