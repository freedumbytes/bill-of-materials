package nl.demon.shadowland.freedumbytes.rest.test;


import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import nl.demon.shadowland.freedumbytes.rest.config.JerseyConfigBuilder;

import lombok.extern.slf4j.Slf4j;


/**
 * {@code JerseySpringIT} uses {@code JerseyTest} internally and configures the client {@code ClientConfig} based on the server {@code ResourceConfig}.
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class JerseySpringIT implements ApplicationContextAware
{
  private static final String SPRING_APPLICATION_CONTEXT_KEY = "contextConfig";

  private ApplicationContext applicationContext;
  private JerseyTest jerseyTest;


  @Before
  public void setup() throws Exception
  {
    getJerseyTest().setUp();
  }


  @After
  public void tearDown() throws Exception
  {
    getJerseyTest().tearDown();
  }


  public final WebTarget target(String path)
  {
    return getJerseyTest().target(path);
  }


  protected ApplicationContext getApplicationContext()
  {
    return applicationContext;
  }


  @Override
  public void setApplicationContext(ApplicationContext applicationContext)
  {
    this.applicationContext = applicationContext;
  }


  public JerseyTest getJerseyTest()
  {
    if (jerseyTest == null)
    {
      jerseyTest = createJerseyTest();
    }

    return jerseyTest;
  }


  private JerseyTest createJerseyTest()
  {
    return new JerseyTest()
    {
      @Override
      protected Application configure()
      {
        return JerseySpringIT.this.configure();
      }


      @Override
      protected void configureClient(ClientConfig clientConfig)
      {
        JerseySpringIT.this.configureClient(clientConfig);
      }
    };
  }


  protected Application configure()
  {
    ResourceConfig serverConfig = getResourceConfigServer();

    if (reusedSpringWiredResourceConfig(serverConfig))
    {
      log.info("Property 'contextConfig' already set thus probably the Spring wired one.");

      return serverConfig;
    }
    else
    {
      serverConfig.property(SPRING_APPLICATION_CONTEXT_KEY, getApplicationContext());
    }

    return serverConfig;
  }


  protected void configureClient(ClientConfig clientConfig)
  {
    ResourceConfig serverConfig = getResourceConfigServer();
    new JerseyConfigBuilder().configureClientBasedOnServer(clientConfig, serverConfig);
  }


  private static boolean reusedSpringWiredResourceConfig(ResourceConfig serverConfig)
  {
    return (serverConfig.getProperty(SPRING_APPLICATION_CONTEXT_KEY) != null);
  }


  protected abstract ResourceConfig getResourceConfigServer();
}
