package nl.demon.shadowland.freedumbytes.java.util.logging.manager;


import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.slf4j.LoggerFactory;


/**
 * Alternative Slf4j Logger for {@code java.util.logging} that doesn't require {@code SLF4JBridgeHandler} and uses {@link #isLoggable} thus removing the performance issue of the bridge handler.
 *
 * <p>
 * Should be used in combination with {@link Slf4jLogManager}.
 * </p>
 *
 * @see <a href="https://www.slf4j.org/legacy.html" target="_blank" rel="noopener noreferrer">Bridging legacy APIs</a>
 * @see <a href="https://stackoverflow.com/questions/4121722/how-to-make-jersey-to-use-slf4j-instead-of-jul" target="_blank" rel="noopener noreferrer">How to make Jersey to use SLF4J instead of JUL?</a>
 * @see <a href="https://github.com/eclipse-ee4j/jersey/tree/master/core-common/src/main/java/org/glassfish/jersey/logging" target="_blank" rel="noopener noreferrer">Eclipse EE4J Jersey logging</a>
 * @see <a href="https://github.com/apache/cxf/tree/master/core/src/main/java/org/apache/cxf/common/logging" target="_blank" rel="noopener noreferrer">Apache CXF logging</a>
 */
public class Slf4jLoggerWrapper extends Logger
{
  private static final int TRACE_LEVEL_THRESHOLD = Level.FINEST.intValue();
  private static final int DEBUG_LEVEL_THRESHOLD = Level.FINE.intValue();
  private static final int INFO_LEVEL_THRESHOLD = Level.INFO.intValue();
  private static final int WARN_LEVEL_THRESHOLD = Level.WARNING.intValue();


  public Slf4jLoggerWrapper(Logger logger)
  {
    super(logger.getName(), (logger.getResourceBundle() == null ? null : logger.getResourceBundle().getBaseBundleName()));
  }


  @Override
  public boolean isLoggable(Level julLevel)
  {
    boolean isLoggable;
    int levelValue = julLevel.intValue();

    if (levelValue <= TRACE_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isTraceEnabled();
    }
    else if (levelValue <= DEBUG_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isDebugEnabled();
    }
    else if (levelValue <= INFO_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isInfoEnabled();
    }
    else if (levelValue <= WARN_LEVEL_THRESHOLD)
    {
      isLoggable = getLogger().isWarnEnabled();
    }
    else
    {
      isLoggable = getLogger().isErrorEnabled();
    }

    return isLoggable;
  }


  @Override
  public void log(LogRecord record)
  {
    int levelValue = record.getLevel().intValue();
    String i18nMessage = getMessageI18N(record);
    Throwable thrown = record.getThrown();

    if (levelValue <= TRACE_LEVEL_THRESHOLD)
    {
      getLogger().trace(i18nMessage, thrown);
    }
    else if (levelValue <= DEBUG_LEVEL_THRESHOLD)
    {
      getLogger().debug(i18nMessage, thrown);
    }
    else if (levelValue <= INFO_LEVEL_THRESHOLD)
    {
      getLogger().info(i18nMessage, thrown);
    }
    else if (levelValue <= WARN_LEVEL_THRESHOLD)
    {
      getLogger().warn(i18nMessage, thrown);
    }
    else
    {
      getLogger().error(i18nMessage, thrown);
    }
  }


  /**
   * Get the record's message, possibly via a resource bundle.
   *
   * @param record
   * @return
   */
  private static String getMessageI18N(LogRecord record)
  {
    String message = record.getMessage();

    if (message == null)
    {
      return null;
    }

    ResourceBundle bundle = record.getResourceBundle();

    if (bundle != null)
    {
      try
      {
        message = bundle.getString(message);
      }
      catch (MissingResourceException e)
      {
        /* NOP */
      }
    }

    Object[] params = record.getParameters();

    if (params != null && params.length > 0)
    {
      try
      {
        message = MessageFormat.format(message, params);
      }
      catch (IllegalArgumentException e)
      {
        /* NOP */
      }
    }

    return message;
  }


  private org.slf4j.Logger getLogger()
  {
    return LoggerFactory.getLogger(getName());
  }
}
