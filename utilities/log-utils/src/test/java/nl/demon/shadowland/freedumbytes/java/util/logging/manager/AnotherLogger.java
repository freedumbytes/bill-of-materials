package nl.demon.shadowland.freedumbytes.java.util.logging.manager;


import java.util.logging.Logger;


public class AnotherLogger extends Logger
{
  public AnotherLogger()
  {
    super(AnotherLogger.class.getName(), null);
  }
}
