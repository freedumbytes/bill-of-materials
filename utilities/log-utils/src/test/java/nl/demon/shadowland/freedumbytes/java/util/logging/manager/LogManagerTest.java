package nl.demon.shadowland.freedumbytes.java.util.logging.manager;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;

import lombok.extern.java.Log;


@Log
public class LogManagerTest
{
  @Rule
  public LoggerRule loggerRule = new LoggerRule(Slf4jLogManager.class.getPackage().getName());


  @Test
  public void isInstalled()
  {
    Slf4jLogManager.isInstalled();

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents();

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.INFO);
    assertThat(events.get(0).getMessage()).isEqualTo("Slf4jLogManager is NOT installed.");
  }


  @Test
  public void addLoggerAnnotation()
  {
    assertThat(log).isExactlyInstanceOf(Logger.class);
  }


  @Test
  public void addLogger()
  {
    SomeLogger logger = new SomeLogger();
    assertThat(Slf4jLogManager.getLogManager().addLogger(logger)).isTrue();

    Logger someLogger = Slf4jLogManager.getLogManager().getLogger(SomeLogger.class.getName());

    assertThat(someLogger).isNotNull();

    assertThat(someLogger).isInstanceOf(Logger.class);
    assertThat(someLogger).isExactlyInstanceOf(SomeLogger.class);
    assertThat(someLogger).isNotInstanceOf(Slf4jLoggerWrapper.class);
  }


  @Test
  public void addSlf4jLoggerWrapper()
  {
    int retryCounter = 0;
    Logger anotherLogger = null;

    while (anotherLogger == null)
    {
      retryCounter++;
      assertThat(Slf4jLogManager.getLogManager().addLogger(new Slf4jLoggerWrapper(new AnotherLogger()))).isTrue();

      anotherLogger = Slf4jLogManager.getLogManager().getLogger(AnotherLogger.class.getName());
    }

    assertThat(anotherLogger).isNotNull();

    anotherLogger.log(Level.INFO, "RetryCounter: {0}", retryCounter);

    assertThat(anotherLogger).isInstanceOf(Logger.class);
    assertThat(anotherLogger).isNotInstanceOf(AnotherLogger.class);
    assertThat(anotherLogger).isExactlyInstanceOf(Slf4jLoggerWrapper.class);
  }
}
