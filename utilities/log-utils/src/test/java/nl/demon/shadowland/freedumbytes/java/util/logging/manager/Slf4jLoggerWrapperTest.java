package nl.demon.shadowland.freedumbytes.java.util.logging.manager;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

import org.apache.log4j.spi.LoggingEvent;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import nl.demon.shadowland.freedumbytes.unit.rule.LoggerRule;

import lombok.extern.java.Log;


@Log
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class Slf4jLoggerWrapperTest
{
  private static final String SOME_MESSAGE = "some message";
  private static final String NO_MESSAGE = null;
  private static final String PARAM_MESSAGE = "some message with param {0,number,#}";
  private static final String BUNDLE_NAME = "theBundle";
  private static final String BUNDLE_PROPERTY_NAME = "property-name";
  private static final String BUNDLE_PROPERTY_VALUE = "property-value";
  private static final String BUNDLE_PROPERTY_MISSING = "property-missing";

  @Rule
  public LoggerRule loggerRule = new LoggerRule(Slf4jLoggerWrapperTest.class.getName());

  @Rule
  public LoggerRule someLoggerRule = new LoggerRule(SomeLogger.class.getName());

  @Mock
  private ResourceBundle resourceBundle;


  @Test
  public void logWithTrace()
  {
    loggerRule.withLevel(org.apache.log4j.Level.TRACE);

    createSlf4jLoggerWrapper().log(Level.FINEST, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINER, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINE, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.CONFIG, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.INFO, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.WARNING, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.SEVERE, SOME_MESSAGE);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(7));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.TRACE);
    assertThat(events.get(0).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.log4j.Level.DEBUG);
    assertThat(events.get(1).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(2).getLevel()).isEqualTo(org.apache.log4j.Level.DEBUG);
    assertThat(events.get(2).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(3).getLevel()).isEqualTo(org.apache.log4j.Level.INFO);
    assertThat(events.get(3).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(4).getLevel()).isEqualTo(org.apache.log4j.Level.INFO);
    assertThat(events.get(4).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(5).getLevel()).isEqualTo(org.apache.log4j.Level.WARN);
    assertThat(events.get(5).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(6).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(6).getMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logWithDebug()
  {
    loggerRule.withLevel(org.apache.log4j.Level.DEBUG);

    createSlf4jLoggerWrapper().log(Level.FINEST, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINER, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINE, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.CONFIG, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.INFO, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.WARNING, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.SEVERE, SOME_MESSAGE);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(6));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.DEBUG);
    assertThat(events.get(0).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.log4j.Level.DEBUG);
    assertThat(events.get(1).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(2).getLevel()).isEqualTo(org.apache.log4j.Level.INFO);
    assertThat(events.get(2).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(3).getLevel()).isEqualTo(org.apache.log4j.Level.INFO);
    assertThat(events.get(3).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(4).getLevel()).isEqualTo(org.apache.log4j.Level.WARN);
    assertThat(events.get(4).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(5).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(5).getMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logWithInfo()
  {
    loggerRule.withLevel(org.apache.log4j.Level.INFO);

    createSlf4jLoggerWrapper().log(Level.FINEST, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINER, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINE, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.CONFIG, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.INFO, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.WARNING, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.SEVERE, SOME_MESSAGE);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(4));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.INFO);
    assertThat(events.get(0).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.log4j.Level.INFO);
    assertThat(events.get(1).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(2).getLevel()).isEqualTo(org.apache.log4j.Level.WARN);
    assertThat(events.get(2).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(3).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(3).getMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logWithWarn()
  {
    loggerRule.withLevel(org.apache.log4j.Level.WARN);

    createSlf4jLoggerWrapper().log(Level.FINEST, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINER, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINE, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.CONFIG, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.INFO, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.WARNING, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.SEVERE, SOME_MESSAGE);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(2));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.WARN);
    assertThat(events.get(0).getMessage()).isEqualTo(SOME_MESSAGE);

    assertThat(events.get(1).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(1).getMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logWithError()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    createSlf4jLoggerWrapper().log(Level.FINEST, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINER, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.FINE, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.CONFIG, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.INFO, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.WARNING, SOME_MESSAGE);
    createSlf4jLoggerWrapper().log(Level.SEVERE, SOME_MESSAGE);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo(SOME_MESSAGE);
  }


  @Test
  public void logNoMessage()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    createSlf4jLoggerWrapper().log(Level.SEVERE, NO_MESSAGE);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isNull();
  }


  @Test
  public void logParamMessage()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    createSlf4jLoggerWrapper().log(Level.SEVERE, PARAM_MESSAGE, 1);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo("some message with param 1");
  }


  @Test
  public void logInvalidParamMessage()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    createSlf4jLoggerWrapper().log(Level.SEVERE, PARAM_MESSAGE, "no-number");

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo(PARAM_MESSAGE);
  }


  @Test
  public void logNullParamMessage()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    createSlf4jLoggerWrapper().log(Level.SEVERE, PARAM_MESSAGE, (Object[]) null);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo(PARAM_MESSAGE);
  }


  @Test
  public void logEmptyParamMessage()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    createSlf4jLoggerWrapper().log(Level.SEVERE, PARAM_MESSAGE, new Object[0]);

    List<LoggingEvent> events = loggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo(PARAM_MESSAGE);
  }


  @Test
  public void logResourceBundleMessage()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    when(resourceBundle.getBaseBundleName()).thenReturn(BUNDLE_NAME);

    createSlf4jLoggerWrapper(resourceBundle).log(Level.SEVERE, BUNDLE_PROPERTY_NAME);

    List<LoggingEvent> events = someLoggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo(BUNDLE_PROPERTY_VALUE);
  }


  @Test
  public void logResourceBundleMissingPropertyMessage()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    when(resourceBundle.getBaseBundleName()).thenReturn(BUNDLE_NAME);

    createSlf4jLoggerWrapper(resourceBundle).log(Level.SEVERE, BUNDLE_PROPERTY_MISSING);

    List<LoggingEvent> events = someLoggerRule.verifyLoggingEvents(times(1));

    assertThat(events.get(0).getLevel()).isEqualTo(org.apache.log4j.Level.ERROR);
    assertThat(events.get(0).getMessage()).isEqualTo(BUNDLE_PROPERTY_MISSING);
  }


  @Test
  public void isLoggableWithTrace()
  {
    loggerRule.withLevel(org.apache.log4j.Level.TRACE);

    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINEST)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINER)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINE)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.CONFIG)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.INFO)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.WARNING)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.SEVERE)).isTrue();
  }


  @Test
  public void isLoggableWithDebug()
  {
    loggerRule.withLevel(org.apache.log4j.Level.DEBUG);

    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINEST)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINER)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINE)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.CONFIG)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.INFO)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.WARNING)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.SEVERE)).isTrue();
  }


  @Test
  public void isLoggableWithInfo()
  {
    loggerRule.withLevel(org.apache.log4j.Level.INFO);

    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINEST)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINER)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINE)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.CONFIG)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.INFO)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.WARNING)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.SEVERE)).isTrue();
  }


  @Test
  public void isLoggableWithWarn()
  {
    loggerRule.withLevel(org.apache.log4j.Level.WARN);

    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINEST)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINER)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINE)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.CONFIG)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.INFO)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.WARNING)).isTrue();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.SEVERE)).isTrue();
  }


  @Test
  public void isLoggableWithError()
  {
    loggerRule.withLevel(org.apache.log4j.Level.ERROR);

    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINEST)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINER)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINE)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.CONFIG)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.INFO)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.WARNING)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.SEVERE)).isTrue();
  }


  @Test
  public void isLoggableWithOff()
  {
    loggerRule.withLevel(org.apache.log4j.Level.OFF);

    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINEST)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINER)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.FINE)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.CONFIG)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.INFO)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.WARNING)).isFalse();
    assertThat(createSlf4jLoggerWrapper().isLoggable(Level.SEVERE)).isFalse();
  }


  private Slf4jLoggerWrapper createSlf4jLoggerWrapper(ResourceBundle resourceBundle)
  {
    SomeLogger someLogger = new SomeLogger();
    someLogger.setResourceBundle(resourceBundle);

    return new Slf4jLoggerWrapper(someLogger);
  }


  private Slf4jLoggerWrapper createSlf4jLoggerWrapper()
  {
    return new Slf4jLoggerWrapper(log);
  }
}
