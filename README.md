
[comment]: # (Project specific information)

# Bill Of Materials

[![Bill Of Materials License](https://img.shields.io/badge/Apache_License-2.0-orange.svg "Bill Of Materials License")](https://www.apache.org/licenses/LICENSE-2.0.html)
[![Bill Of Materials pipeline](https://gitlab.com/freedumbytes/bill-of-materials/badges/master/build.svg "Bill Of Materials pipeline")](https://gitlab.com/freedumbytes/bill-of-materials)

[![Bill Of Materials Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Bill Of Materials Maven Site")](https://freedumbytes.gitlab.io/bill-of-materials/)
[![Bill Of Materials Maven Site](https://javadoc.io/badge/nl.demon.shadowland.freedumbytes.maven.dependencies/bom.svg?color=yellow&label=Bill%20Of%20Materials "Bill Of Materials Maven Site")](https://freedumbytes.gitlab.io/bill-of-materials/)

[![Bill Of Materials Maven Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Bill Of Materials Maven Central")](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22nl.demon.shadowland.freedumbytes.maven.dependencies%22)
[![Bill Of Materials Maven Central](https://img.shields.io/maven-central/v/nl.demon.shadowland.freedumbytes.maven.dependencies/bom.svg?label=Maven%20Central "Bill Of Materials Maven Central")](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22nl.demon.shadowland.freedumbytes.maven.dependencies%22)

[![Bill Of Materials Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Bill Of Materials Nexus")](https://oss.sonatype.org/index.html#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.dependencies~~~~)
[![Bill Of Materials Nexus](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.dependencies/bom.svg?label=Nexus "Bill Of Materials Nexus")](https://oss.sonatype.org/index.html#nexus-search;gav~nl.demon.shadowland.freedumbytes.maven.dependencies~~~~)

[![Bill Of Materials MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "Bill Of Materials MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.dependencies)
[![Bill Of Materials MvnRepository](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.dependencies/bom.svg?label=MvnRepository "Bill Of Materials MvnRepository")](https://mvnrepository.com/artifact/nl.demon.shadowland.freedumbytes.maven.dependencies)

[![Bill Of Materials SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "Bill Of Materials SonarCloud")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom)
[![Bill Of Materials Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=alert_status "Bill Of Materials Quality Gate")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom)
[![Bill Of Materials vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=vulnerabilities "Bill Of Materials vulnerabilities")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=vulnerabilities)
[![Bill Of Materials bugs](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=bugs "Bill Of Materials bugs")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=bugs)
[![Bill Of Materials coverage](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=coverage "Bill Of Materials coverage")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=coverage)

[![Bill Of Materials SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "Bill Of Materials SonarCloud")](https://sonarcloud.io/dashboard?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom)
[![Bill Of Materials lines of code](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=ncloc "Bill Of Materials lines of code")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=ncloc)
[![Bill Of Materials duplication](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=duplicated_lines_density "Bill Of Materials duplication")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=duplicated_lines)
[![Bill Of Materials technical debt](https://sonarcloud.io/api/project_badges/measure?project=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=sqale_index "Bill Of Materials technical debt")](https://sonarcloud.io/component_measures?id=nl.demon.shadowland.freedumbytes.maven.dependencies:bom&metric=sqale_index)

[![Bill Of Materials Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Bill Of Materials Dependency Check")](https://freedumbytes.gitlab.io/bill-of-materials/dependency-check-report.html)
[![Bill Of Materials Dependency Check](https://img.shields.io/nexus/r/https/oss.sonatype.org/nl.demon.shadowland.freedumbytes.maven.dependencies/bom.svg?label=Dependency%20Check "Bill Of Materials Dependency Check")](https://freedumbytes.gitlab.io/bill-of-materials/dependency-check-report.html)

[![Bill Of Materials Javadoc.io](https://freedumbytes.gitlab.io/setup/images/icon/javadoc.png "Bill Of Materials Javadoc.io")](https://javadoc.io/doc/nl.demon.shadowland.freedumbytes.maven.dependencies/bom)
[![Bill Of Materials Javadoc.io](https://javadoc.io/badge/nl.demon.shadowland.freedumbytes.maven.dependencies/bom.svg?color=yellow&label=Javadoc "Bill Of Materials Javadoc.io")](https://javadoc.io/doc/nl.demon.shadowland.freedumbytes.maven.dependencies/bom)

  * [Maven Dependencies Grouping](https://freedumbytes.gitlab.io/bill-of-materials/grouping/index.html).
  * [Maven Grouping Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/index.html).
  * [Dependency Updates Report](https://freedumbytes.gitlab.io/bill-of-materials/dependency-updates-report.html).

[comment]: # (Project specific information)

#### Legend

  * [![Maven Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Maven Site") Maven Site](https://maven.apache.org/plugins/maven-site-plugin/) generated site also includes the project's reports that were configured in the POM.
  * [![Central](https://freedumbytes.gitlab.io/setup/images/icon/central.png "Central") The Central Repository](https://search.maven.org/).
  * [![Nexus](https://freedumbytes.gitlab.io/setup/images/icon/nexus.png "Nexus") Nexus Repository Manager](https://oss.sonatype.org/index.html).
  * [![MvnRepository](https://freedumbytes.gitlab.io/setup/images/icon/mvnrepository.png "MvnRepository") MvnRepository](https://mvnrepository.com/) is like Google for Maven artifacts with Dependencies Updates information.
  * [![SonarCloud](https://freedumbytes.gitlab.io/setup/images/icon/sonarcloud.png "SonarCloud") SonarCloud](https://sonarcloud.io/explore/projects?sort=-analysis_date) is a widely adopted open source platform to inspect continuously the quality of source code and detect bugs, vulnerabilities and code smells in more than 20 different languages.
  * [![Dependency Check](https://freedumbytes.gitlab.io/setup/images/icon/dependency-check.png "Dependency Check") Dependency Check](https://www.owasp.org/index.php/OWASP_Dependency_Check) is a utility that identifies project dependencies and checks if there are any known, publicly disclosed, vulnerabilities.

# Available Documentation

Overall project documentation can be found at:

 * [![Bitbucket](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket") freedumbytes.bitbucket.io](https://freedumbytes.bitbucket.io/index.xhtml)
 * [![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") freedumbytes.github.io](https://freedumbytes.github.io/index.xhtml)
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") freedumbytes.gitlab.io](https://freedumbytes.gitlab.io/index.xhtml)

Those sites can also be found through my Home Page [![Shadowland](https://freedumbytes.gitlab.io/setup/images/icon/mike-muir.png "Shadowland") www.shadowland.demon.nl](http://www.shadowland.demon.nl/index.xhtml).

# Available Projects

Currently using the following git repositories:

 * [![Bitbucket](https://freedumbytes.gitlab.io/setup/images/icon/bitbucket.png "Bitbucket") Bitbucket](https://bitbucket.org/freedumbytes/)
   ([System Metrics](https://status.bitbucket.org) or [![Bitbucket Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "Bitbucket Twitter") @bitbucketstatus](https://twitter.com/bitbucketstatus))
 * [![GitHub](https://freedumbytes.gitlab.io/setup/images/icon/github.png "GitHub") GitHub](https://github.com/freedumbytes/)
   ([Status Messages](https://status.github.com) or [![GitHub Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "GitHub Twitter") @githubstatus](https://twitter.com/githubstatus))
 * [![GitLab](https://freedumbytes.gitlab.io/setup/images/icon/gitlab.png "GitLab") GitLab](https://gitlab.com/freedumbytes/)
   ([System Metrics](https://status.gitlab.com) or [![GitLab Twitter](https://freedumbytes.gitlab.io/setup/images/icon/twitter.png "GitLab Twitter") @gitlabstatus](https://twitter.com/gitlabstatus))

Check the following [git command examples](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#vcs.git.commands).

# Development Production Line - The Short Story

The [Development Production Line](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html) manual (as of version `4.0-beta`) contains a description how the following projects came about:

 1. [Maven Setup](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.setup) the Maven site, license and plugin configuration in a parent `POM`.

 2. [Maven Versions Rules](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.versions.rules) the Maven `ruleSet` file containing the rules that control how to compare version numbers.

    **Note**: The `rules.xml` was almost removed with version `3.0.0` of `versions-maven-plugin` (see also [issue 157](https://github.com/mojohaus/versions-maven-plugin/issues/157)).

 3. [Custom Dependency Check](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.dependency.check.custom.suppression.hints) `suppressionFile` and `hintsFile` containing the rules that control how to suppress false positives and resolve false negatives.

 4. [Bill Of Materials](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.grouping.dependencies) to setup the required versions of the most popular Maven dependencies and group them logically together.

 5. [Custom Maven Skins](https://freedumbytes.gitlab.io/dpl/html/complete-manual.html#prj.maven.site.skin.custom) configuration.

 6. [JModalWindow](https://freedumbytes.gitlab.io/jmodalwindow.xhtml) was created to support modal functionality, similar to `JDialog`, without blocking all frames. And was migrated from Ant to Maven to demonstrate `maven.compiler.target` usage.

 7. [Basketball](https://gitlab.com/freedumbytes/basketball/) Data Storage of Game Statistics.

#### 1. Maven Setup

To use the site, license and plugin configuration of [Maven Setup](https://gitlab.com/freedumbytes/setup):

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven</groupId>
    <artifactId>setup</artifactId>
    <version>x.y.z</version>
  </parent>
```

To also use the Java build and reporting settings:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven</groupId>
    <artifactId>java</artifactId>
    <version>x.y.z</version>
  </parent>
```

Plus the following subset of the `setup` configured properties:

```xml
  <properties>
    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    <maven.compiler.source>${maven.compiler.target}</maven.compiler.source>
    <maven.compiler.target>${maven.compiler.compilerVersion}</maven.compiler.target>
    <maven.compiler.testSource>${maven.compiler.testTarget}</maven.compiler.testSource>
    <maven.compiler.testTarget>${maven.compiler.target}</maven.compiler.testTarget>
    <!--
      Requires Java9+, otherwise: 'mvn compile' results in "javac: invalid flag: --release".
      <maven.compiler.release>8</maven.compiler.release>
    -->

    <mojoSignatureArtifactId>java18</mojoSignatureArtifactId>
    <ignoreDependencies>true</ignoreDependencies>

    <extraEnforcerRulesMaxJdkVersion>${maven.compiler.target}</extraEnforcerRulesMaxJdkVersion>
    <extraEnforcerRulesFail>true</extraEnforcerRulesFail>

    <javadocVersion>1.8.0</javadocVersion>
    <javadocDoclint>none</javadocDoclint>

    <aggregateSurefireReports>true</aggregateSurefireReports>
    <surefire.useSystemClassLoader>true</surefire.useSystemClassLoader>
    <failsafe.useSystemClassLoader>true</failsafe.useSystemClassLoader>
  </properties>
```

Those properties can be changed when:

  * Compiling for older JDKs.
  * Compiling and testing with different source levels.
  * Single module projects should use `aggregateSurefireReports` `false`, otherwise the Surefire report will always show that the number of tests is `0` and the Failsafe report will be missing altogether.
  * Error: Could not find or load main class org.apache.maven.surefire.booter.ForkedBooter (see also [Class Loading and Forking in Maven Surefire](https://maven.apache.org/surefire/maven-surefire-plugin/examples/class-loading.html)).


#### 2. Maven Versions Rules

To use the ruleSet file containing the [Versions Rules](https://gitlab.com/freedumbytes/versions-rules) that control how to compare version numbers:

```xml
  <properties>
    <ossrhHost>https://oss.sonatype.org</ossrhHost>
    <nexusHost>${ossrhHost}</nexusHost>

    <versionsRulesVersion>x.y.z</versionsRulesVersion>
    <versionsRulesFolderVersion>${versionsRulesVersion}</versionsRulesFolderVersion>
    <versionsRulesPath>
      ${nexusHost}/content/groups/public/nl/demon/shadowland/freedumbytes/maven/versioning/versions-rules/${versionsRulesFolderVersion}
    </versionsRulesPath>
  </properties>

  <profiles>
    <profile>
      <id>enableUpdatesReports</id>

      <reporting>
        <plugins>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>versions-maven-plugin</artifactId>
            <reportSets>
              <reportSet>
                <reports>
                  <report>dependency-updates-report</report>
                  <report>plugin-updates-report</report>
                  <report>property-updates-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>versions-maven-plugin</artifactId>
          <version>${versionsMavenPluginVersion}</version>
          <configuration>
            <outputEncoding>${project.reporting.outputEncoding}</outputEncoding>
            <rulesUri>${versionsRulesPath}/versions-rules-${versionsRulesVersion}.xml</rulesUri>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

#### 3. Custom Dependency Check

To use the [Custom Dependency Check](https://gitlab.com/freedumbytes/dependency-check) `suppressionFile` and `hintsFile` containing the rules that control how to suppress false positives and resolve false negatives:

```xml
  <properties>
    <ossrhHost>https://oss.sonatype.org</ossrhHost>
    <nexusHost>${ossrhHost}</nexusHost>

    <owaspReportsPath>${project.build.directory}/owasp-reports</owaspReportsPath>
    <owaspReportFormat>all</owaspReportFormat>
    <cveValidForHours>24</cveValidForHours>
    <skipDependencyManagement>true</skipDependencyManagement>

    <customSuppressionHintsVersion>x.y.z</customSuppressionHintsVersion>
    <customSuppressionHintsFolderVersion>${customSuppressionHintsVersion}</customSuppressionHintsFolderVersion>
    <customSuppressionHintsPath>
      ${nexusHost}/content/groups/public/nl/demon/shadowland/freedumbytes/maven/owasp/dependency-check/${customSuppressionHintsFolderVersion}
    </customSuppressionHintsPath>
    <customSuppressionFile>
      ${customSuppressionHintsPath}/dependency-check-${customSuppressionHintsVersion}-suppression.xml
    </customSuppressionFile>
    <customHintsFile>
      ${customSuppressionHintsPath}/dependency-check-${customSuppressionHintsVersion}-hints.xml
    </customHintsFile>
  </properties>

  <profiles>
    <profile>
      <id>enableDependencyCheckReport</id>

      <reporting>
        <plugins>
          <plugin>
            <groupId>org.owasp</groupId>
            <artifactId>dependency-check-maven</artifactId>
            <version>${mavenDependencyCheckVersion}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>aggregate</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.owasp</groupId>
          <artifactId>dependency-check-maven</artifactId>
          <version>${mavenDependencyCheckVersion}</version>
          <configuration>
            <name>Dependency Check Report</name>
            <outputDirectory>${owaspReportsPath}</outputDirectory>
            <format>${owaspReportFormat}</format>
            <cveValidForHours>${cveValidForHours}</cveValidForHours>
            <skipDependencyManagement>${skipDependencyManagement}</skipDependencyManagement>

            <suppressionFile>${customSuppressionFile}</suppressionFile>
            <hintsFile>${customHintsFile}</hintsFile>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

##### Sample Report Showing Vulnerable Dependencies

![Dependency Check Sample Default](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-default.png "Dependency Check Sample Default")

 **Note**: Since `OWASP Dependency Check Plugin` release `3.1.0` (see also [pull request 1028](https://github.com/jeremylong/DependencyCheck/pull/1028) False Positive Reduction) these custom suppression and hints files are obsolete
 unless the `Vulnerabilities` should show up as `Suppressed`.

![Dependency Check Sample Custom](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom.png "Dependency Check Sample Custom")

##### Sample Report Showing All Dependencies

![Dependency Check Sample Custom All](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-custom-all.png "Dependency Check Sample Custom All")

##### Sample Report Showing Custom Suppressed Vulnerabilities

![Dependency Check Sample Custom Suppressed Collapsed](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-collapsed.png "Dependency Check Sample Custom Suppressed Collapsed")

![Dependency Check Sample Custom Suppressed Expanded](https://freedumbytes.gitlab.io/setup/images/screenshot/dependency-check-sample-suppressed-vulnerabilities-expanded.png "Dependency Check Sample Custom Suppressed Expanded")

#### 4. Bill Of Materials

To define the required versions [BOM](https://gitlab.com/freedumbytes/bill-of-materials) of the most popular Maven dependencies:

```xml
  <properties>
    <bomVersion>x.y.z</bomVersion>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>nl.demon.shadowland.freedumbytes.maven.dependencies</groupId>
        <artifactId>bom</artifactId>
        <version>${bomVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
```

To include Grouping Log/Test Dependencies:

```xml
  <properties>
    <bomVersion>x.y.z</bomVersion>
  </properties>

  <dependencies>
    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.dependencies</groupId>
      <artifactId>log</artifactId>
      <type>pom</type>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>nl.demon.shadowland.freedumbytes.maven.dependencies</groupId>
      <artifactId>test</artifactId>
      <type>pom</type>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

**Note**: Use scope test to restrict the dependencies to the test phase only.

Overview of Maven Dependencies Groupings and their content:

  * [Common](https://freedumbytes.gitlab.io/bill-of-materials/grouping/common/index.html)
  * [Spring](https://freedumbytes.gitlab.io/bill-of-materials/grouping/spring/index.html)
  * [Hibernate](https://freedumbytes.gitlab.io/bill-of-materials/grouping/hibernate/index.html)
  * [Jersey](https://freedumbytes.gitlab.io/bill-of-materials/grouping/jersey/index.html)
  * [Test](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/index.html)
  * [Log](https://freedumbytes.gitlab.io/bill-of-materials/grouping/log/index.html)
  * [MySQL](https://freedumbytes.gitlab.io/bill-of-materials/grouping/mysql/index.html)
  * [Tomcat](https://freedumbytes.gitlab.io/bill-of-materials/grouping/tomcat/index.html)

##### 4.1 [Monkey Island](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/about.html)

The term Mock Objects has become a popular one to describe special case objects that mimic real objects for testing.

Expect-run-verify is the most common pattern in mock frameworks. [What's wrong with it](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/expect-run-verify-goodbye.html)?
[Can I test what I want, please](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/can-i-test-what-i-want-please.html)? I'm a mockist but existing mock frameworks just don't appeal to me.
They spoil my TDD experience. They harm code readability. I needed something better. That's why I came up with [Mockito](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/mockito.html).

I'm about to show that it is very useful to distinguish two basic kinds of interactions: [asking an object of data and telling an object to do something](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/is-there-a-difference-between-asking-and-telling.html).
Fair enough, it may sound obvious but most mocking frameworks treat all interactions equally and don't take advantage of the distinction.

By now, you've probably figured out that Mockito is not strictly a mocking framework. It's rather a spying/stubbing framework but that's a story for a [different post](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/monkey-island/lets-spy.html).

  * Mockito - [The New Mock Framework on the Block](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/behind-the-times/the-new-mock-framework-on-the-block.html).
  * [Mocks and Stubs aren't Spies](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/behind-the-times/mocks-and-stubs-arent-spies.html).
  * [Mocks Aren't Stubs](https://freedumbytes.gitlab.io/bill-of-materials/grouping/test/backup/martin-fowler/mocks-arent-stubs.html).

##### 4.2 [Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/index.html)

Overview of Maven Grouping Utilities and their content:

  * [Log Test Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/log-test-utils/index.html)
    * Just a default sample [log4j.xml](https://gitlab.com/freedumbytes/bill-of-materials/blob/master/utilities/log-test-utils/src/main/resources/log4j.xml) useful for testing the other projects.
  * [Test Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/test-utils/index.html)
    * [LoggerRule](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/unit/rule/LoggerRule.html) with assertions for Log4j logging behaviour in unit tests.
  * [Log Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/log-utils/index.html)
    * [Slf4jLoggingFeatureLevelWrapper](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/java/util/logging/jersey/Slf4jLoggingFeatureLevelWrapper.html) alternative `Logger` for Jersey `LoggingFeature` to handle all logging of package or class as if of supplied level and uses `isLoggable` to check if `log4j.xml` is set to this level or higher.
    * [Slf4jLogManager](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/java/util/logging/manager/Slf4jLogManager.html) custom LogManager for `java.util.logging` that doesn't require `SLF4JBridgeHandler` and uses [Slf4jLoggerWrapper](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/java/util/logging/manager/Slf4jLoggerWrapper.html)`.isLoggable` thus removing the performance issue of the bridge handler.
  * [Tomcat Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/tomcat-utils/index.html)
    * [SessionTimeoutListener](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/servlet/http/listener/session/timeout/SessionTimeoutListener.html) a sample `@WebListener` Session Timeout Logger.
    * [SessionKillerFilter](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/servlet/http/filter/session/killer/SessionKillerFilter.html) a sample `@WebFilter` to see `SessionTimeoutListener` in action.
  * [Jersey Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/jersey-utils/index.html)
    * [JAXB Adapters](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/rest/adapter/package-summary.html) of Java 8 Local Date/Time API ISO format for custom marshaling.
    * [QueryParam Converters](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/rest/param/package-summary.html) of Java 8 Local Date/Time API ISO format for custom converting of a message parameter value and the corresponding custom Java type.
    * [JerseyConfigBuilder](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/rest/config/package-summary.html) with often used Jersey Server/Client Configuration settings.
  * [Jersey Test Utilities](https://freedumbytes.gitlab.io/bill-of-materials/utilities/jersey-test-utils/index.html)
    * [JerseyIT](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/rest/test/JerseyIT.html) extends `JerseyTest` and configures the client `ClientConfig` based on the server `ResourceConfig`.
    * [JerseySpringIT](https://freedumbytes.gitlab.io/bill-of-materials/apidocs/nl/demon/shadowland/freedumbytes/rest/test/JerseySpringIT.html) uses `JerseyTest` internally and configures the client `ClientConfig` based on the server `ResourceConfig`.

To use the custom `Slf4jLogManager` for example in Maven edit the `pom.xml` as follows:

```xml
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <argLine>-Djava.util.logging.manager=nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager ${argLine}</argLine>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
        <configuration>
          <argLine>-Djava.util.logging.manager=nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager ${argLine}</argLine>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

**Note**: When using the JaCoCo Plugin don't forget the extra `${argLine}` option to supply its agent.

This will result in some extra never noticed before logging:

```
2018-12-24 19:47:48,264 INFO  [main] internal.ServiceFinder  - Running in a non-OSGi environment
2018-12-24 19:47:48,285 INFO  [main] test.JerseyTest  - Found multiple TestContainerFactory service providers, using the default found {0}
2018-12-24 19:47:48,599 INFO  [main] server.ApplicationHandler  - Initiating Jersey application, version Jersey: 2.27 2018-04-10 07:34:57...
2018-12-24 19:47:49,033 INFO  [main] internal.ExecutorProviders  - Selected ExecutorServiceProvider implementation [org.glassfish.jersey.server.ServerExecutorProvidersConfigurator$DefaultManagedAsyncExecutorProvider] to be used for injection of executor qualified by [org.glassfish.jersey.server.ManagedAsyncExecutor] annotation.
2018-12-24 19:47:49,034 INFO  [main] internal.ExecutorProviders  - Selected ScheduledExecutorServiceProvider implementation [org.glassfish.jersey.server.ServerExecutorProvidersConfigurator$DefaultBackgroundSchedulerProvider] to be used for injection of scheduler qualified by [org.glassfish.jersey.server.BackgroundScheduler] annotation.
2018-12-24 19:47:49,372 INFO  [main] server.ApplicationHandler  - Jersey application initialized.
Root Resource Classes:
  nl.demon.shadowland.freedumbytes.rest.test.spring.EchoResource
Pre-match Filters:
   org.glassfish.jersey.logging.ServerLoggingFilter
Global Response Filters:
   org.glassfish.jersey.logging.ServerLoggingFilter
Global Reader Interceptors:
   org.glassfish.jersey.server.internal.MappableExceptionWrapperInterceptor
Global Writer Interceptors:
   org.glassfish.jersey.logging.ServerLoggingFilter
   org.glassfish.jersey.server.internal.MappableExceptionWrapperInterceptor
   org.glassfish.jersey.server.internal.JsonWithPaddingInterceptor
Message Body Readers:
   org.glassfish.jersey.moxy.json.internal.ConfigurableMoxyJsonProvider
Message Body Writers:
   org.glassfish.jersey.moxy.json.internal.ConfigurableMoxyJsonProvider
```

To test both `LogManager` and `Slf4jLogManager` use for example the following `pom.xml` setup:

```xml
  <properties>
    <jacoco.append>true</jacoco.append>
  </properties>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <excludes>
            <exclude>**/LogManagerTest.java</exclude>
            <exclude>**/Slf4jLogManagerTest.java</exclude>
          </excludes>
        </configuration>
        <executions>
          <execution>
            <id>surefire-jul</id>
            <phase>test</phase>
            <goals>
              <goal>test</goal>
            </goals>
            <configuration>
              <includes>
                <include>**/LogManagerTest.java</include>
              </includes>
              <excludes>
                <exclude>**/Slf4jLogManagerTest.java</exclude>
              </excludes>
              <argLine>-Djava.util.logging.manager=java.util.logging.LogManager ${argLine}</argLine>
            </configuration>
          </execution>
          <execution>
            <id>surefire-slf4j</id>
            <phase>test</phase>
            <goals>
              <goal>test</goal>
            </goals>
            <configuration>
              <includes>
                <include>**/Slf4jLogManagerTest.java</include>
              </includes>
              <excludes>
                <exclude>**/LogManagerTest.java</exclude>
              </excludes>
              <argLine>-Djava.util.logging.manager=nl.demon.shadowland.freedumbytes.java.util.logging.manager.Slf4jLogManager ${argLine}</argLine>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
```

**Note**: To prevent the default `excludes` from reverting the `include` add an `exclude` also.

#### 5. Custom Maven Skins

To use for example the [Custom Reflow Skin](https://gitlab.com/freedumbytes/skins):

```xml
  <properties>
    <skinGroupId>nl.demon.shadowland.freedumbytes.maven.custom.skins</skinGroupId>
    <skinArtifactId>maven-reflow-skin</skinArtifactId>
    <skinVersion>${customMavenReflowSkinVersion}</skinVersion>

    <customSkinVersion>x.y.z</customSkinVersion>
    <customMavenPaperSkinVersion>${customSkinVersion}</customMavenPaperSkinVersion>
    <customMavenFluidoSkinVersion>${customSkinVersion}</customMavenFluidoSkinVersion>
    <customMavenReflowSkinVersion>${customSkinVersion}</customMavenReflowSkinVersion>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>${mavenSitePluginVersion}</version>
          <dependencies>
            <dependency>
              <groupId>nl.demon.shadowland.freedumbytes.patch.lt.velykis.maven.skins</groupId>
              <artifactId>reflow-velocity-tools</artifactId>
              <version>${customMavenReflowSkinVersion}</version>
            </dependency>
          </dependencies>
          <configuration>
            <generateSitemap>${generateSitemap}</generateSitemap>
            <inputEncoding>${project.build.sourceEncoding}</inputEncoding>
            <outputEncoding>${project.reporting.outputEncoding}</outputEncoding>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
```

And the corresponding site.xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/DECORATION/1.8.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/DECORATION/1.8.0 https://maven.apache.org/xsd/decoration-1.8.0.xsd"
         combine.self="override">
  <skin>
    <groupId>${skinGroupId}</groupId>
    <artifactId>${skinArtifactId}</artifactId>
    <version>${skinVersion}</version>
  </skin>

  <publishDate position="bottom" format="yyyy-MM-dd HH:mm:ss z" />
  <version position="navigation-bottom" />

  <custom>
    <reflowSkin>
      <localResources>true</localResources>

      <highlightJs>true</highlightJs>

      <topNav>parent|modules|reports</topNav>
      <navbarInverse>true</navbarInverse>

      <breadcrumbs>false</breadcrumbs>
      <toc>false</toc>

      <skinAttribution>false</skinAttribution>
    </reflowSkin>
  </custom>

  <body>
    <menu inherit="bottom" ref="parent" />
    <menu inherit="bottom" ref="modules" />
    <menu inherit="bottom" ref="reports" />
  </body>
</project>
```

#### 6. JModalWindow

  * [![JModalWindow SUN Article](https://freedumbytes.gitlab.io/setup/images/icon/jmodalwindow.png "JModalWindow SUN Article")](https://freedumbytes.gitlab.io/jmodalwindow.xhtml) [JModalWindow SUN Article](https://freedumbytes.gitlab.io/jmodalwindow.xhtml) about the [JModalWindow Project](https://gitlab.com/freedumbytes/jmodalwindow).
  * Options to run the SUN Article Sample and the newer complete Demo (don't forget to run `mvn compile` first):
    * `mvn exec:exec -pl sample`
    * `mvn exec:exec -pl demo [-Dextra.argument=-minimal]`

**Note**: The optional command line option `extra.argument` with value `-minimal` disables the blurring of a blocked window and the busy cursor, when moving the mouse cursor over it. It also disables iconify of a blocked internal frame.

#### 7. Basketball

  * [![Basketball](https://freedumbytes.gitlab.io/setup/images/icon/basketball.png "Basketball")](https://gitlab.com/freedumbytes/basketball/) [Basketball](https://gitlab.com/freedumbytes/basketball/) Data Storage of Game Statistics to demonstrate:
    * How to use this OpenSource project structure setup.
    * [Eclipse Jersey](https://projects.eclipse.org/projects/ee4j.jersey/) REST framework.
    * [React JavaScript](https://reactjs.org/) library for building user interfaces.

#### 8. SonarQube/SonarCloud Maven Report Plugin

Add the [SonarQube/SonarCloud Maven Report Plugin](https://github.com/SonarQubeCommunity/sonar-maven-report) to the reporting section in the POM:

```xml
<project>
  …

  <reporting>
    <plugins>
      <plugin>
        <groupId>nl.demon.shadowland.maven.plugins</groupId>
        <artifactId>sonarqube-maven-report</artifactId>
        <version>0.2.x</version>
      </plugin>
    </plugins>
  </reporting>
</project>
```

